package dlesiak.interview.app;

import dlesiak.interview.api.CheckoutApiConfig;
import dlesiak.interview.product.CatalogueConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@Import({CatalogueConfiguration.class, CheckoutApiConfig.class}) // <-- AppConfig imports DataSourceConfig
public class Application {

    @Bean //TODO move to specific module?
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
