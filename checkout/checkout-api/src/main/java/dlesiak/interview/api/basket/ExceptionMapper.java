package dlesiak.interview.api.basket;

import dlesiak.interview.checkout.basket.exception.BasketException;
import dlesiak.interview.checkout.basket.exception.CheckoutError;

import static dlesiak.interview.checkout.basket.exception.CheckoutError.*;

class ExceptionMapper {

    AppError map(BasketException ex) {
        CheckoutError error = ex.getError();
        switch (error) {
            case NO_PRODUCT_FOUND:
                return new AppError(
                        NO_PRODUCT_FOUND.toString(),
                        "No product found in catalogue!"
                );
            case BASKET_CLOSED:
                return new AppError(
                        BASKET_CLOSED.toString(),
                        "Basket is already checkout out!"
                );
            case EMPTY_BASKET:
                return new AppError(
                        EMPTY_BASKET.toString(),
                        "Nothing in basket!"
                );
            case NO_OPENED_BASKET:
                return new AppError(
                        NO_OPENED_BASKET.toString(),
                        "No opened basket found!"
                );
            case BASKET_ALREADY_OPENED:
                return new AppError(
                        BASKET_ALREADY_OPENED.toString(),
                        "Basket already opened!"
                );
            default:
                return new AppError(
                        "OTHER",
                        "Something went wrong!"
                );
        }
    }
}
