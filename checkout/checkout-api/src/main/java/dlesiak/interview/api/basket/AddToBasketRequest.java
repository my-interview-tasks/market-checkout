package dlesiak.interview.api.basket;

import lombok.Getter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Getter
class AddToBasketRequest {
    @NotBlank
    private String productId;
    @Min(value = 1)
    private int quantity;
    @NotBlank
    private String customerId;
}
