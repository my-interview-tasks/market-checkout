package dlesiak.interview.api.basket;

import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
class RemoveFromBasketRequest {
    @NotBlank
    private String productId;
    @NotBlank
    private String customerId;
}
