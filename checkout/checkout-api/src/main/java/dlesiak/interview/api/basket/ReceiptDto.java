package dlesiak.interview.api.basket;

import dlesiak.interview.checkout.receipt.Receipt;
import dlesiak.interview.checkout.receipt.ReceiptItem;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.stream.Collectors;

@Getter
@Setter
class ReceiptDto {
    private int total;
    private int discount;
    private int totalAfterDiscount;
    private Collection<ReceiptItemDto> items;

    static ReceiptDto toDto(Receipt receipt) {
        ReceiptDto receiptDto = new ReceiptDto();
        receiptDto.setItems(ReceiptItemDto.from(receipt.getReceiptItems()));
        receiptDto.setTotal(receipt.getTotal().toInteger());
        receiptDto.setDiscount(receipt.getDiscount().toInteger());
        receiptDto.setTotalAfterDiscount(receipt.getAfterDiscount().toInteger());
        return receiptDto;
    }

    @Getter
    @Setter
    static class ReceiptItemDto {
        private String name;
        private String code;
        private int quantity;
        private int total;
        private int price;

        static Collection<ReceiptItemDto> from(Collection<ReceiptItem> receiptItems) {
            return receiptItems
                    .stream()
                    .map(ReceiptItemDto::from)
                    .collect(Collectors.toList());
        }

        private static ReceiptItemDto from(ReceiptItem receiptItem) {
            ReceiptItemDto receiptItemDto = new ReceiptItemDto();
            receiptItemDto.setName(receiptItem.getProductName());
            receiptItemDto.setCode(receiptItem.getProductCode().getCode());
            receiptItemDto.setQuantity(receiptItem.getQuantity());
            receiptItemDto.setPrice(receiptItem.getPrice().toInteger());
            receiptItemDto.setTotal(receiptItem.getTotal().toInteger());
            return receiptItemDto;
        }

    }

}
