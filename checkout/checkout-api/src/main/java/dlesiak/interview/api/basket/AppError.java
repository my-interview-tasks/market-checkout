package dlesiak.interview.api.basket;

public final class AppError {
    private final String code;
    private final String description;

    public AppError(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
