package dlesiak.interview.api;

import dlesiak.interview.checkout.BasketQueryService;
import dlesiak.interview.checkout.Cashier;
import dlesiak.interview.checkout.basket.BasketFactory;
import dlesiak.interview.checkout.basket.BasketRepository;
import dlesiak.interview.checkout.product.ProductRepository;
import dlesiak.interview.checkout.promotion.PromotionRepository;
import dlesiak.interview.checkout.receipt.ReceiptGenerator;
import dlesiak.interview.checkout.receipt.ReceiptRepository;
import dlesiak.interview.infrastructure.checkout.CheckoutInfrastructureConfig;
import dlesiak.interview.shared.event.EventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = {"dlesiak.interview.api.basket"})
@Import(CheckoutInfrastructureConfig.class)
public class CheckoutApiConfig {

    @Bean
    BasketFactory basketFactory(PromotionRepository promotionRepository) {
        return new BasketFactory(promotionRepository);
    }

    @Bean
    BasketQueryService queryService(BasketRepository basketRepository) {
        return new BasketQueryService(basketRepository);
    }

    @Bean
    Cashier cashier(BasketRepository basketRepository, ProductRepository productRepository, ReceiptRepository receiptRepository, EventPublisher eventPublisher, BasketFactory basketFactory) {
        return new Cashier(
                basketRepository,
                productRepository,
                receiptRepository,
                new ReceiptGenerator(),
                basketFactory,
                eventPublisher
        );
    }

}
