package dlesiak.interview.api.basket;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Getter
class BasketTotalDto {
    @NotNull
    private Integer value;
}
