package dlesiak.interview.api.basket;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.BasketItem;
import lombok.Getter;

import java.util.Collection;
import java.util.stream.Collectors;

@Getter
class BasketDto {
    private int id;
    private int total;
    private int totalAfterDiscount;
    private Collection<BasketItemDto> basketItems;

    @Getter
    static class BasketItemDto {
        private String code;
        private String name;
        private int price;
        private int quantity;
        private int total;
        private int totalAfterDiscount;


        static BasketItemDto toDto(BasketItem basketItem) {
            BasketItemDto basketItemDto = new BasketItemDto();
            basketItemDto.code = basketItem.getProduct().getProductCode().getCode();
            basketItemDto.name = basketItem.getProduct().getName();
            basketItemDto.price = basketItem.getProduct().getPrice().toInteger();
            basketItemDto.quantity = basketItem.getQuantity();
            basketItemDto.totalAfterDiscount = basketItem.total().toInteger();
            basketItemDto.total = basketItem.totalWithoutDiscount().toInteger();
            return basketItemDto;
        }
    }

    static BasketDto toDto(Basket basket) {
        Collection<BasketItemDto> items = basket
                .getBasketItems()
                .stream()
                .map(BasketItemDto::toDto)
                .collect(Collectors.toList());

        BasketDto basketDto = new BasketDto();
        basketDto.id = basket.getId();
        basketDto.total = basket.totalWithoutDiscount().toInteger();
        basketDto.totalAfterDiscount = basket.total().toInteger();
        basketDto.basketItems = items;
        return basketDto;
    }
}
