package dlesiak.interview.api.basket;

import dlesiak.interview.checkout.BasketQueryService;
import dlesiak.interview.checkout.Cashier;
import dlesiak.interview.checkout.basket.CustomerId;
import dlesiak.interview.checkout.basket.exception.BasketException;
import dlesiak.interview.checkout.product.ProductCode;
import dlesiak.interview.checkout.receipt.Receipt;
import dlesiak.interview.shared.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/basket")
public class BasketController {

    private final ExceptionMapper exceptionMapper = new ExceptionMapper();
    private final Cashier cashier;
    private final BasketQueryService basketQueryService;

    @Autowired
    public BasketController(Cashier cashier, BasketQueryService basketQueryService) {
        this.cashier = cashier;
        this.basketQueryService = basketQueryService;
    }

    @GetMapping
    Collection<BasketDto> getBaskets(
            @RequestParam(name = "customerId") @Valid @NotBlank String customerId,
            @RequestParam(name = "status") @Valid @NotNull Status status
    ) {
        switch (status) {
            case CLOSED:
                return basketQueryService
                        .getClosedBaskets(new CustomerId(customerId))
                        .stream()
                        .map(BasketDto::toDto)
                        .collect(Collectors.toList());
            case OPENED:
                return basketQueryService
                        .getOpenedBasket(new CustomerId(customerId))
                        .map(BasketDto::toDto)
                        .map(Collections::singletonList)
                        .orElse(Collections.emptyList());
            default:
                throw new IllegalArgumentException();
        }
    }

    @PutMapping()
    ResponseEntity openBasket(@RequestParam(name = "customerId") @Valid @NotBlank String customerId) {
        cashier.openBasket(customerId);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/item")
    ResponseEntity<BasketTotalDto> addItemToBasket(@RequestBody @Valid AddToBasketRequest request) {
        Money total = cashier.addOrUpdateBasketItem(
                request.getCustomerId(),
                new ProductCode(request.getProductId()),
                request.getQuantity());
        return ResponseEntity.ok().body(new BasketTotalDto(total.toInteger()));
    }

    @DeleteMapping("/item")
    ResponseEntity<BasketTotalDto> removeItemFromBasket(
            @RequestParam @Valid String customerId,
            @RequestParam @Valid String productCode
    ) {
        Money total = cashier.removeBasketItem(
                customerId,
                new ProductCode(productCode));
        return ResponseEntity.ok().body(new BasketTotalDto(total.toInteger()));
    }

    @PostMapping("/checkout")
    ResponseEntity<ReceiptDto> checkout(@RequestParam(name = "customerId") @Valid @NotBlank String customerId) {
        Receipt receipt = cashier.checkout(customerId);
        return ResponseEntity.ok().body(ReceiptDto.toDto(receipt));
    }

    @ExceptionHandler(value = BasketException.class)
    ResponseEntity<AppError> handleEx(BasketException ex) {
        return ResponseEntity.badRequest().body(exceptionMapper.map(ex));
    }

    enum Status {
        OPENED, CLOSED
    }
}
