package dlesiak.interview.api.basket

import com.fasterxml.jackson.databind.ObjectMapper
import dlesiak.interview.checkout.BasketQueryService
import dlesiak.interview.checkout.Cashier
import dlesiak.interview.checkout.basket.CustomerId
import dlesiak.interview.checkout.basket.exception.*
import dlesiak.interview.checkout.product.ProductCode
import dlesiak.interview.checkout.receipt.Receipt
import dlesiak.interview.checkout.receipt.ReceiptItem
import dlesiak.interview.shared.Money
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.lang.Unroll
import spock.mock.DetachedMockFactory

import java.time.LocalDateTime

import static groovy.json.JsonOutput.toJson
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest
@ContextConfiguration(classes = Config)
@Unroll
class BasketControllerSpec extends Specification {

    @Autowired
    private MockMvc mvc

    @Autowired
    private Cashier cashier

    @Autowired
    private BasketQueryService queryService

    @Autowired
    ObjectMapper objectMapper

    def "get closed baskets"() {
        when:
        def response = mvc.perform(get('/basket')
                .param("customerId", "customer")
                .param("status", "CLOSED"))

        then: "response is ok"
        response.andExpect(status().isOk())

        and:
        1 * queryService.getClosedBaskets(new CustomerId("customer")) >> []
        0 * queryService.getOpenedBasket(new CustomerId("customer")) >> Optional.empty()
    }

    def "get opened basket"() {
        when:
        def response = mvc.perform(get('/basket')
                .param("customerId", "customer")
                .param("status", "OPENED"))

        then: "response is ok"
        response.andExpect(status().isOk())

        and:
        1 * queryService.getOpenedBasket(new CustomerId("customer")) >> Optional.empty()
        0 * queryService.getClosedBaskets(new CustomerId("customer")) >> []
    }

    def "opening new basket returns 200"() {
        given: "customerId request param"
        def customerId = "customer"

        when:
        def response = mvc.
                perform(put('/basket')
                        .param("customerId", customerId))

        then: "response is ok"
        response.andExpect(status().isOk())
    }

    def "adding new item to basket returns total"() {
        given: "request"
        Map request = [
                productId : 'coffee',
                quantity  : 2,
                customerId: 'janNowak'
        ]

        and: "total of basket"
        def total = Money.of(100)
        cashier.addOrUpdateBasketItem('janNowak', new ProductCode('coffee'), 2) >> total

        when:
        def response = mvc.
                perform(post('/basket/item')
                        .contentType(APPLICATION_JSON)
                        .content(toJson(request)))

        then: "response is ok"
        response.andExpect(status().isOk())

        and: "total of basket is returned"
        with(objectMapper.readValue(response.andReturn().response.contentAsString, Map)) {
            it.value == total.toInteger()
        }
    }

    def "adding new item with invalid request returns 400"() {
        when:
        def response = mvc.perform(post('/basket/item')
                .contentType(APPLICATION_JSON)
                .content(toJson(invalidRequest)))

        then: "bad request"
        response.andExpect(status().isBadRequest())

        where: "invalid requests"
        invalidRequest << [
                //quantity == 0
                [
                        customerId: 'janNowak',
                        productId : "coffee",
                        quantity  : 0
                ],
                //no productId
                [
                        customerId: 'janNowak',
                        quantity  : 1
                ],
                //empty productId
                [
                        customerId: 'janNowak',
                        productId : "",
                        quantity  : 1
                ],
                //empty customerId
                [
                        customerId: '',
                        productId : "coffee",
                        quantity  : 1
                ],
                //no customerId
                [
                        productId: "coffee",
                        quantity : 1
                ],
                //empty request
                [] as Map
        ]
    }

    def "removing item from basket returns basket total"() {
        given:
        def total = Money.of(80)
        cashier.removeBasketItem('janNowak', new ProductCode('coffee')) >> total

        when:
        def response = mvc.perform(delete('/basket/item')
                .param("customerId", "janNowak")
                .param("productCode", "coffee"))

        then: "response is ok"
        response.andExpect(status().isOk())

        and: "total is returned"
        with(objectMapper.readValue(response.andReturn().response.contentAsString, Map)) {
            it.value == 80
        }
    }

    def "removing item from basket with invalid request returns 400"() {
        when:
        def response = mvc.perform(delete('/basket/item')
                .contentType(APPLICATION_JSON)
                .content(toJson(request)))

        then: "bad request"
        response.andExpect(status().isBadRequest())

        where: "invalid requests"
        request << [
                //empty request
                [] as Map,
                //empty productId
                [
                        productId : "",
                        customerId: "JanNowak"
                ],
                //no productId
                [
                        customerId: "JanNowak"
                ],
                //empty customerId
                [
                        productId : "coffee",
                        customerId: ""
                ],
                //no customerId
                [
                        productId: "coffee",
                ]
        ]
    }

    def "checkout basket returns 200"() {
        given: "receipt"
        def customerId = new CustomerId("JanNowak")
        def receiptItems = [
                new ReceiptItem(
                        "TEA",
                        new ProductCode('tea-code'),
                        10,
                        Money.of(10),
                        Money.of(100)),
                new ReceiptItem(
                        "COFFEE",
                        new ProductCode('coffee-code'),
                        3,
                        Money.of(101),
                        Money.of(401)),
                new ReceiptItem(
                        "VODKA",
                        new ProductCode("vodka-code"),
                        4,
                        Money.of(110),
                        Money.of(330)),
        ]
        def receipt = Receipt.builder()
                .customerId(customerId)
                .receiptItems(receiptItems)
                .total(Money.of(100))
                .afterDiscount(Money.of(90))
                .discount(Money.of(10))
                .timestamp(LocalDateTime.now())
                .build()

        cashier.checkout("JanNowak") >> receipt

        when: "checkout"
        def response = mvc.perform(post('/basket/checkout')
                .param("customerId", "JanNowak"))

        then: "response is ok"
        response.andExpect(status().isOk())

        and: "receipt is returned"
        with(objectMapper.readValue(response.andReturn().response.contentAsString, Map)) {
            it.total == 100
            it.totalAfterDiscount == 90
            it.discount == 10
            it.items.size() == 3

            it.items[0].name == 'TEA'
            it.items[0].code == 'tea-code'
            it.items[0].quantity == 10
            it.items[0].total == 100
            it.items[0].price == 10

            it.items[1].name == 'COFFEE'
            it.items[1].code == 'coffee-code'
            it.items[1].quantity == 3
            it.items[1].total == 401
            it.items[1].price == 101

            it.items[2].name == 'VODKA'
            it.items[2].code == 'vodka-code'
            it.items[2].quantity == 4
            it.items[2].total == 330
            it.items[2].price == 110
        }
    }

    def "return #error when BasketException is thrown"() {
        given: "throws exception"
        cashier.removeBasketItem('janNowak', new ProductCode('coffee')) >> { throw ex }

        when: "request"
        def response = mvc.perform(delete('/basket/item')
                .param("productCode", "coffee")
                .param("customerId", "janNowak"))

        then: "response is bad request"
        response.andExpect(status().isBadRequest())

        and: "error is returned"
        with(objectMapper.readValue(response.andReturn().response.contentAsString, Map)) {
            it.code == error
        }

        where:
        ex                                 | error
        new NoOpenedBasketFoundException() | "NO_OPENED_BASKET"
        new EmptyBasketException()         | "EMPTY_BASKET"
        new BasketClosedException()        | "BASKET_CLOSED"
        new NoProductFoundException()      | "NO_PRODUCT_FOUND"
        new BasketAlreadyOpenedException() | "BASKET_ALREADY_OPENED"
    }


    @ComponentScan(basePackageClasses = [BasketController])
    static class Config {
        DetachedMockFactory detachedMockFactory = new DetachedMockFactory()

        @Bean
        Cashier cashier() {
            return detachedMockFactory.Stub(Cashier)
        }

        @Bean
        BasketQueryService queryService() {
            return detachedMockFactory.Mock(BasketQueryService)
        }

    }
}
