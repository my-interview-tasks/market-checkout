package dlesiak.interview.infrastructure.checkout;

import dlesiak.interview.checkout.product.Product;
import dlesiak.interview.checkout.product.ProductCode;
import dlesiak.interview.checkout.product.ProductRepository;
import dlesiak.interview.shared.Money;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Repository
@AllArgsConstructor
public class ProductRepositoryImpl implements ProductRepository {

    //TODO as ENVIRONMENT VARIABLE
    private final static String URL = "http://localhost:8083/product?code=";
    private final RestTemplate restTemplate;

    @Override
    public Optional<Product> getProduct(ProductCode productCode) {
        String url = URL + productCode.getCode();

        //TODO resttemplate throws error if 404. set exception handler.
        try {
            ResponseEntity<ProductDto> forEntity = restTemplate.getForEntity(url, ProductDto.class);
            ProductDto dto = forEntity.getBody();
            return Optional.of(
                    new Product(
                            new ProductCode(dto.getCode()),
                            dto.getName(),
                            Money.of(dto.getPrice())));
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                return Optional.empty();
            } else {
                throw e;
            }

        }
    }

    @Getter
    private static class ProductDto {
        private String code;
        private String name;
        private int price;
    }

}
