package dlesiak.interview.infrastructure.checkout.recipe;

import dlesiak.interview.checkout.product.ProductCode;
import dlesiak.interview.checkout.receipt.ReceiptItem;
import dlesiak.interview.shared.Money;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
class ReceiptItemEntity {
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Column(name = "PRODUCT_CODE")
    private String productCode;
    @Column(name = "QUANTITY")
    private int quantity;
    @Column(name = "PRICE")
    private int price;
    @Column(name = "TOTAL")
    private int total;

    static ReceiptItemEntity map(ReceiptItem receiptItem) {
        return new ReceiptItemEntity(
                receiptItem.getProductCode().getCode(),
                receiptItem.getProductName(),
                receiptItem.getQuantity(),
                receiptItem.getPrice().toInteger(),
                receiptItem.getTotal().toInteger()
        );
    }

    ReceiptItem toDto() {
        return ReceiptItem.builder()
                .productName(productName)
                .productCode(new ProductCode(productCode))
                .quantity(quantity)
                .total(Money.of(total))
                .price(Money.of(price))
                .build();
    }
}
