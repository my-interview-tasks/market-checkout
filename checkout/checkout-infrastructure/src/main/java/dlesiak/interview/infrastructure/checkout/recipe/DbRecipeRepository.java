package dlesiak.interview.infrastructure.checkout.recipe;

import dlesiak.interview.checkout.basket.CustomerId;
import dlesiak.interview.checkout.receipt.Receipt;
import dlesiak.interview.checkout.receipt.ReceiptRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class DbRecipeRepository implements ReceiptRepository {

    private final ReceiptDao receiptDao;

    @Autowired
    public DbRecipeRepository(ReceiptDao receiptDao) {
        this.receiptDao = receiptDao;
    }

    @Override
    public void save(Receipt receipt) {
        receiptDao.save(ReceiptEntity.toEntity(receipt));
    }

    @Override
    public Optional<Receipt> findLastReceipt(CustomerId customerId) {
        return receiptDao.findByCustomerId(customerId.getId())
                .map(ReceiptEntity::toReceipt);
    }
}
