package dlesiak.interview.infrastructure;

import dlesiak.interview.shared.event.Event;
import dlesiak.interview.shared.event.EventPublisher;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class StubEventPublisher implements EventPublisher {

    @Override
    public void publish(Collection<Event> events) {
        //TODO implement
        System.out.println("EVENT PUBLISHED");
        events.forEach(System.out::println);
    }
}
