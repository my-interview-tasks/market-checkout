package dlesiak.interview.infrastructure.checkout;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.BasketRepository;
import dlesiak.interview.checkout.basket.CustomerId;
import dlesiak.interview.checkout.basket.exception.BasketAlreadyOpenedException;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Repository
public class InMemoryBasketRepository implements BasketRepository {

    private final Map<Integer, Basket> baskets = new ConcurrentHashMap<>();
    private final AtomicInteger sequence = new AtomicInteger();

    // !!! Database has constraint, only one open basket per customerId !!!
    @Override
    public void saveBasket(Basket basket) {

        if (basket.isOpen() && customerHasAlreadyOpenedDifferentBasket(basket)) {
            throw new BasketAlreadyOpenedException();
        }

        if (basket.getId() == 0) {
            int id = sequence.incrementAndGet();
            basket.setId(id);
            baskets.put(id, basket);
        } else {
            baskets.put(basket.getId(), basket);
        }
    }

    private boolean customerHasAlreadyOpenedDifferentBasket(Basket basket) {
        return baskets
                .values()
                .stream()
                .filter(b -> b.getCustomerId().equals(basket.getCustomerId()))
                .filter(b -> b.getId() != basket.getId())
                .anyMatch(Basket::isOpen);
    }

    @Override
    public Optional<Basket> getOpenedBasket(CustomerId customerId) {
        return baskets.values()
                .stream()
                .filter(basket -> basket.getCustomerId().equals(customerId))
                .filter(Basket::isOpen)
                .findAny();
    }

    @Override
    public Collection<Basket> getClosedBaskets(CustomerId customerId) {
        return baskets.values()
                .stream()
                .filter(basket -> basket.getCustomerId().equals(customerId))
                .filter(Basket::isClosed)
                .collect(Collectors.toList());
    }

}
