package dlesiak.interview.infrastructure.checkout;

import dlesiak.interview.checkout.product.ProductCode;
import dlesiak.interview.checkout.promotion.Promotion;
import dlesiak.interview.checkout.promotion.PromotionFactory;
import dlesiak.interview.checkout.promotion.PromotionRepository;
import dlesiak.interview.shared.Money;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Repository
public class PromotionRepositoryImpl implements PromotionRepository {

    private Collection<Promotion> promotions = new ArrayList<>();

    {
        PromotionFactory factory = new PromotionFactory();
        promotions.add(
                factory.bundlePromotion(
                        "Buy 3 x Tea and pay 70$",
                        new ProductCode("product-tea"),
                        3,
                        Money.of(70)));
        promotions.add(
                factory.bundlePromotion(
                        "Buy 2 x Coffee and pay 15$",
                        new ProductCode("product-coffee"),
                        2,
                        Money.of(15)));
        promotions.add(
                factory.bundlePromotion(
                        "Buy 4 x Vodka and pay 60$",
                        new ProductCode("product-vodka"),
                        4,
                        Money.of(60)));
        promotions.add(
                factory.bundlePromotion(
                        "Buy 2 x Cola and pay 25$",
                        new ProductCode("product-cola"),
                        2,
                        Money.of(40)));

        //TODO Uncomment when multiplepromotion strategy is implemented.
//        promotions.add(
//                factory.percentDiscountBasketItem(
//                        "Buy Coffee and Tea and get 10% discount",
//                        Arrays.asList(new ProductCode("product-tea"), new ProductCode("product-coffee")),
//                        10
//                ));
//        promotions.add(
//                factory.percentDiscountBasketItem(
//                        "Buy Vodka and Cola and get 30% discount",
//                        Arrays.asList(new ProductCode("product-cola"), new ProductCode("product-vodka")),
//                        30
//                ));
//
//        promotions.add(
//                factory.fixedWholeBasketDiscount(
//                        "Buy Vodka and Cola and get 25$ discount",
//                        Arrays.asList(new ProductCode("product-cola"), new ProductCode("product-vodka")),
//                        Money.of(25)
//                ));
//        promotions.add(
//                factory.fixedWholeBasketDiscount(
//                        "Buy Tea and get 3$ discount",
//                        Arrays.asList(new ProductCode("product-tea")),
//                        Money.of(3)
//                ));

    }

    @Override
    public Collection<Promotion> getPromotions() {
        return Collections.unmodifiableCollection(promotions);
    }
}
