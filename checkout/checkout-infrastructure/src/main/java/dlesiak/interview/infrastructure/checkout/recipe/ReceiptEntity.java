package dlesiak.interview.infrastructure.checkout.recipe;

import dlesiak.interview.checkout.basket.CustomerId;
import dlesiak.interview.checkout.receipt.Receipt;
import dlesiak.interview.checkout.receipt.ReceiptItem;
import dlesiak.interview.shared.Money;
import lombok.Builder;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "RECEIPT")
@Builder
class ReceiptEntity {
    @Id
    @GeneratedValue
    private long id;
    @Column(name = "CUSTOMER_ID")
    private String customerId;
    @Column(name = "TIMESTAMP")
    private LocalDateTime timestamp;
    @Column(name = "TOTAL")
    private int total;
    @Column(name = "DISCOUNT")
    private int discount;
    @Column(name = "TOTAL_AFTER_DISCOUNT")
    private int totalAfterDiscount;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "RECEIPT_ITEMS", joinColumns = @JoinColumn(name = "RECEIPT_ID"))
    private Collection<ReceiptItemEntity> receiptItems;

    static ReceiptEntity toEntity(Receipt receipt) {
        List<ReceiptItemEntity> receiptItems = receipt
                .getReceiptItems()
                .stream()
                .map(ReceiptItemEntity::map)
                .collect(Collectors.toList());

        return ReceiptEntity.builder()
                .customerId(receipt.getCustomerId().getId())
                .timestamp(receipt.getTimestamp())
                .discount(receipt.getDiscount().toInteger())
                .totalAfterDiscount(receipt.getAfterDiscount().toInteger())
                .total(receipt.getTotal().toInteger())
                .receiptItems(receiptItems)
                .build();
    }

    Receipt toReceipt() {
        Collection<ReceiptItem> receiptItems = this.receiptItems
                .stream()
                .map(ReceiptItemEntity::toDto)
                .collect(Collectors.toList());

        return Receipt.builder()
                .timestamp(timestamp)
                .receiptItems(receiptItems)
                .total(Money.of(total))
                .afterDiscount(Money.of(totalAfterDiscount))
                .discount(Money.of(discount))
                .customerId(new CustomerId(customerId))
                .build();
    }

}
