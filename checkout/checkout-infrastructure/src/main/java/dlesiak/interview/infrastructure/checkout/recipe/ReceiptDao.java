package dlesiak.interview.infrastructure.checkout.recipe;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
interface ReceiptDao extends JpaRepository<ReceiptEntity, Long> {

    Optional<ReceiptEntity> findByCustomerId(String customerId);
}
