package dlesiak.interview.infrastructure.checkout;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = {"dlesiak.interview.infrastructure"})
@ComponentScan(basePackages = {"dlesiak.interview.infrastructure"})
@EnableJpaRepositories(basePackages = {"dlesiak.interview.infrastructure"})
public class CheckoutInfrastructureConfig {
}
