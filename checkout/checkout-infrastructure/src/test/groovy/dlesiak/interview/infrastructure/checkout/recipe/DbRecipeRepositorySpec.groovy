package dlesiak.interview.infrastructure.checkout.recipe

import dlesiak.interview.checkout.basket.CustomerId
import dlesiak.interview.checkout.product.ProductCode
import dlesiak.interview.checkout.receipt.Receipt
import dlesiak.interview.checkout.receipt.ReceiptItem
import dlesiak.interview.checkout.receipt.ReceiptRepository
import dlesiak.interview.shared.Money
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

import java.time.LocalDateTime

@DataJpaTest
@ContextConfiguration(classes = [Config])
class DbRecipeRepositorySpec extends Specification {

    @Autowired
    private ReceiptRepository receiptRepository

    @Autowired
    private TestEntityManager testEntityManager

    def 'get receipt from database'() {
        given: "receipt in db"
        def customerId = new CustomerId("customerId")
        def receiptItems = [
                new ReceiptItemEntity("Tea", "tea-code", 10, (10), (100)),
                new ReceiptItemEntity("Coffee", "coffee-code", 4, (101), (401)),
                new ReceiptItemEntity("Vodka", "vodka-code", 3, (110), (330)),
        ]
        def receipt = ReceiptEntity.builder()
                .receiptItems(receiptItems)
                .total(100)
                .discount(10)
                .totalAfterDiscount(90)
                .customerId(customerId.getId())
                .timestamp(LocalDateTime.now())
                .build()

        testEntityManager.persist(receipt)

        when: "read from db"
        def receiptInDb = receiptRepository.findLastReceipt(customerId)

        then:
        receiptInDb.isPresent()
        receiptInDb.get().customerId == customerId
        receiptInDb.get().afterDiscount == Money.of(90)
        receiptInDb.get().total == Money.of(100)
        receiptInDb.get().discount == Money.of(10)
        //TODO test rest
    }


    def 'save receipt'() {
        given: "customerId"
        def customerId = new CustomerId("customerId")

        and: "receipt"
        def receiptItems = [
                new ReceiptItem("Tea", new ProductCode("tea-code"), 10, Money.of(10), Money.of(100)),
                new ReceiptItem("Coffee", new ProductCode("coffee-code"), 4, Money.of(101), Money.of(401)),
                new ReceiptItem("Vodka", new ProductCode("vodka-code"), 3, Money.of(110), Money.of(330)),
        ]
        def receipt = Receipt.builder()
                .receiptItems(receiptItems)
                .total(Money.of(100))
                .discount(Money.of(10))
                .afterDiscount(Money.of(90))
                .customerId(customerId)
                .build()

        when: "saving receipt"
        receiptRepository.save(receipt)

        then: "receipt in db"
        def receiptInDb = receiptRepository.findLastReceipt(customerId)
        receiptInDb.isPresent()
        receiptInDb.get().customerId == customerId
        receiptInDb.get().afterDiscount == Money.of(90)
        receiptInDb.get().total == Money.of(100)
        receiptInDb.get().discount == Money.of(10)
        //TODO test rest
    }

    @EntityScan(basePackages = ["dlesiak.interview.infrastructure.checkout.recipe"])
    @EnableJpaRepositories(basePackages = ["dlesiak.interview.infrastructure.checkout.recipe"])
    @ComponentScan(basePackages = ["dlesiak.interview.infrastructure.checkout.recipe"])
    static class Config {

    }
}
