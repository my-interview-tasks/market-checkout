package dlesiak.interview.infrastructure.checkout

import dlesiak.interview.checkout.product.Product
import dlesiak.interview.checkout.product.ProductCode
import dlesiak.interview.shared.Money

trait ProductFixture {

    static Product COFFEE = new Product(new ProductCode("coffee-code"), "Coffee", Money.of(10))
    static Product TEA = new Product(new ProductCode("coffee-tea"), "tea", Money.of(40))
    static Product VODKA = new Product(new ProductCode("coffee-vodka"), "vodka", Money.of(200))

}