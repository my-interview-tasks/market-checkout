package dlesiak.interview.infrastructure.checkout

import dlesiak.interview.checkout.basket.Basket
import dlesiak.interview.checkout.basket.BasketRepository
import dlesiak.interview.checkout.basket.CustomerId
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.lang.Subject

@Stepwise
class InMemoryBasketRepositorySpec extends Specification implements ProductFixture {

    @Subject
    @Shared
    private BasketRepository basketRepository = new InMemoryBasketRepository()

    @Shared
    def customerId = new CustomerId("JanNowak")

    def "save new opened basket"() {
        given: "new opened basket"
        Basket basket = new Basket(customerId)
        basket.addProduct(TEA, 3)

        and: "no opened basket in repository"
        !basketRepository.getOpenedBasket(customerId).isPresent()

        when: "saving basket"
        basketRepository.saveBasket(basket)

        then: "basket saved"
        basketRepository.getOpenedBasket(customerId).isPresent()

        and: "no closed basket found"
        basketRepository.getClosedBaskets(customerId).size() == 0
    }

    def "update opened basket"() {
        given: "new basket state"
        Basket basket = new Basket(customerId)
        basket.setId(1)
        basket.addProduct(COFFEE, 3)

        when: "saving updated opened basket"
        basketRepository.saveBasket(basket)

        then: "basket is updated"
        def basketInDb = basketRepository.getOpenedBasket(customerId)
        basketInDb.isPresent()
        basketInDb.get().getBasketItems().every({ it.product == COFFEE })

        and: "no closed basket present"
        basketRepository.getClosedBaskets(customerId).size() == 0
    }

    def "return empty if no opened basket"() {
        given: "checkout basket"
        def basket = basketRepository.getOpenedBasket(customerId).get()
        basket.checkout()

        when: "save checkout basket"
        basketRepository.saveBasket(basket)

        then: "no open basket in db"
        !basketRepository.getOpenedBasket(customerId).isPresent()

        and: "closed basket in db"
        basketRepository.getClosedBaskets(customerId).size() == 1
        basketRepository.getClosedBaskets(customerId).every({ it.isClosed() })
    }


    def "save another new basket"() {
        given: "new opened basket"
        Basket basket = new Basket(customerId)
        basket.addProduct(TEA, 3)

        when: "saving basket"
        basketRepository.saveBasket(basket)

        then: "new basket is saved"
        def basketInDb = basketRepository.getOpenedBasket(customerId)
        basketInDb.isPresent()
        basketInDb.get().getId() == 2

        and: "old closed basket is present"
        basketRepository.getClosedBaskets(customerId).size() == 1
    }


}
