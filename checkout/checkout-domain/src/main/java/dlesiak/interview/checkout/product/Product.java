package dlesiak.interview.checkout.product;

import dlesiak.interview.shared.Money;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@EqualsAndHashCode(of = {"productCode"})
@ToString
public final class Product {
    private final ProductCode productCode;
    private final String name;
    private final Money price;
}
