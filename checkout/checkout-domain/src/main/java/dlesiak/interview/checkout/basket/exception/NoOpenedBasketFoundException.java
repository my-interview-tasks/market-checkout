package dlesiak.interview.checkout.basket.exception;

import static dlesiak.interview.checkout.basket.exception.CheckoutError.NO_OPENED_BASKET;

public class NoOpenedBasketFoundException extends BasketException {
    public NoOpenedBasketFoundException() {
        super(NO_OPENED_BASKET);
    }
}
