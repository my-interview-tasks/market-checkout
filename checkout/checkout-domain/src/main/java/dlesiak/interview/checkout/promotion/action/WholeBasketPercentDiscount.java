package dlesiak.interview.checkout.promotion.action;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.Discount;
import dlesiak.interview.shared.Money;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class WholeBasketPercentDiscount implements PromoAction {

    private final int percent;

    @Override
    public void apply(Basket basket) {
        Money value = basket.total().percent(percent);
        basket.addDiscount(new Discount(value));
    }
}
