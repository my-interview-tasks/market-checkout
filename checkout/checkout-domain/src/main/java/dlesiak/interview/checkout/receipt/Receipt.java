package dlesiak.interview.checkout.receipt;


import dlesiak.interview.checkout.basket.CustomerId;
import dlesiak.interview.shared.Money;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

import java.time.LocalDateTime;
import java.util.Collection;

@Builder
@Getter
public class Receipt {

    private final CustomerId customerId;
    private final LocalDateTime timestamp;
    private final Money total;
    private final Money afterDiscount;
    private final Money discount;
    @Singular
    private final Collection<ReceiptItem> receiptItems;

}
