package dlesiak.interview.checkout.receipt;


import dlesiak.interview.checkout.product.ProductCode;
import dlesiak.interview.shared.Money;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ReceiptItem {
    private final String productName;
    private final ProductCode productCode;
    private final int quantity;
    private final Money price;
    private final Money total;
}
