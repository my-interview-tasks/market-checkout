package dlesiak.interview.checkout.basket.exception;

import static dlesiak.interview.checkout.basket.exception.CheckoutError.NO_PRODUCT_FOUND;

public class NoProductFoundException extends BasketException {
    public NoProductFoundException() {
        super(NO_PRODUCT_FOUND);
    }
}
