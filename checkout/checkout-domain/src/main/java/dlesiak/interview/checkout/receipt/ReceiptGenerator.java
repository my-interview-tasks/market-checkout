package dlesiak.interview.checkout.receipt;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.BasketItem;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class ReceiptGenerator {

    public Receipt generate(Basket basket) {
        List<ReceiptItem> receiptItems = basket.getBasketItems()
                .stream()
                .map(this::receiptItem)
                .collect(Collectors.toList());//TODO is total include discount?
        return receipt(basket, receiptItems);
    }

    private Receipt receipt(Basket basket, List<ReceiptItem> receiptItems) {
        return Receipt.builder()
                .customerId(basket.getCustomerId())
                .discount(basket.totalDiscount())
                .afterDiscount(basket.total())
                .total(basket.totalWithoutDiscount())
                .receiptItems(receiptItems)
                .timestamp(LocalDateTime.now())
                .build();
    }

    private ReceiptItem receiptItem(BasketItem basketItem) {
        return ReceiptItem.builder()
                .productName(basketItem.getProduct().getName())
                .productCode(basketItem.getProduct().getProductCode())
                .price(basketItem.getProduct().getPrice())
                .quantity(basketItem.getQuantity())
                .total(basketItem.total())
                .build();
    }
}
