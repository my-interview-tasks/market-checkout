package dlesiak.interview.checkout;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.BasketRepository;
import dlesiak.interview.checkout.basket.CustomerId;
import lombok.AllArgsConstructor;

import java.util.Collection;
import java.util.Optional;

@AllArgsConstructor
public class BasketQueryService {

    private final BasketRepository basketRepository;

    public Optional<Basket> getOpenedBasket(CustomerId customerId) {
        return basketRepository.getOpenedBasket(customerId);
    }

    public Collection<Basket> getClosedBaskets(CustomerId customerId) {
        return basketRepository.getClosedBaskets(customerId);
    }
}
