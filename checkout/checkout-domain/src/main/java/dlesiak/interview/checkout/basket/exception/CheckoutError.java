package dlesiak.interview.checkout.basket.exception;

public enum CheckoutError {
    BASKET_ALREADY_OPENED,
    BASKET_CLOSED,
    NO_OPENED_BASKET,
    NO_PRODUCT_FOUND,
    EMPTY_BASKET
}
