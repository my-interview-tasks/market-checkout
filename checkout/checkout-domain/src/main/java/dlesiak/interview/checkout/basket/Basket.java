package dlesiak.interview.checkout.basket;

import dlesiak.interview.checkout.basket.event.BasketClosed;
import dlesiak.interview.checkout.basket.event.BasketOpened;
import dlesiak.interview.checkout.basket.event.ItemAddedToBasket;
import dlesiak.interview.checkout.basket.event.ItemRemovedFromBasket;
import dlesiak.interview.checkout.basket.exception.BasketClosedException;
import dlesiak.interview.checkout.basket.exception.EmptyBasketException;
import dlesiak.interview.checkout.product.Product;
import dlesiak.interview.checkout.product.ProductCode;
import dlesiak.interview.checkout.promotion.Promotion;
import dlesiak.interview.shared.Money;

import java.util.*;
import java.util.stream.Collectors;

import static dlesiak.interview.shared.Validators.requireNonNull;


public class Basket extends Discounted {

    private int id;
    private final CustomerId customerId;
    private final Collection<BasketItem> basketItems = new HashSet<>();
    private final Collection<Promotion> promotions = new ArrayList<>();
    private BasketStatus basketStatus = BasketStatus.OPEN;

    public Basket(Collection<Promotion> promotions, CustomerId customerId) {
        this(customerId);
        requireNonNull(promotions);
        this.promotions.addAll(promotions);
    }

    public Basket(CustomerId customerId) {
        this.customerId = customerId;
        addEvent(new BasketOpened());
    }

    public boolean isOpen() {
        return basketStatus == BasketStatus.OPEN;
    }

    public boolean isClosed() {
        return basketStatus == BasketStatus.CLOSED;
    }

    public void addProduct(Product product, int quantity) {
        checkIfBasketClosed();
        BasketItem basketItem = findBasketItem(product)
                .map(item -> item.newQuantity(quantity))
                .orElse(new BasketItem(product, quantity));
        basketItems.add(basketItem);
        recalculateDiscounts();
        addEvent(new ItemAddedToBasket());
    }

    public void removeProduct(ProductCode productCode) {
        checkIfBasketClosed();
        this.basketItems
                .stream()
                .filter(basketItem -> basketItem.isForProduct(productCode))
                .findAny()
                .ifPresent(basketItem -> {
                    basketItems.remove(basketItem);
                    recalculateDiscounts();
                    addEvent(new ItemRemovedFromBasket());
                });
    }

    public void checkout() {
        checkIfBasketClosed();
        checkIfBasketIsNotEmpty();
        recalculateDiscounts(); // just to be sure that invariants are met
        close();
    }

    public Collection<Discount> getDiscounts() {
        //TODO REFACTOR (WHEN UPDATE JAVA VERSION OR USE VAVR etc.)
        //TODO USE COMPOSITION INSTEAD OF ABSTRACT CLASS Discounted?
        Collection<Discount> basketDiscounts = super.getDiscounts();
//        Collection<Discount> basketItemDiscounts = getBasketItemDiscounts(); //TODO Rethink discount processing discountValues. THis is method should return total discount of basket and basket Items, right now, it shows total only for basket.
        Collection<Discount> allDiscounts = new ArrayList<>();
        allDiscounts.addAll(basketDiscounts);
//        allDiscounts.addAll(basketItemDiscounts);
        return Collections.unmodifiableCollection(allDiscounts);
    }

    private Collection<Discount> getBasketItemDiscounts() {
        return getBasketItems()
                .stream()
                .map(Discounted::getDiscounts)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public Collection<BasketItem> getBasketItems() {
        return Collections.unmodifiableCollection(basketItems);
    }

    private void checkIfBasketIsNotEmpty() {
        if (basketItems.isEmpty()) {
            throw new EmptyBasketException();
        }
    }

    public Money total() {
        return basketItems
                .stream()
                .map(BasketItem::total)
                .reduce(Money.of(0), Money::sum)
                .minus(totalDiscount());
    }

    public Money totalWithoutDiscount() {
        return basketItems
                .stream()
                .map(BasketItem::totalWithoutDiscount)
                .reduce(Money.of(0), Money::sum);
    }

    public int size() {
        return basketItems.size();
    }

    public CustomerId getCustomerId() {
        return customerId;
    }

    private void close() {
        basketStatus = BasketStatus.CLOSED;
        addEvent(new BasketClosed());
    }

    private void recalculateDiscounts() {
        clearDiscounts();
        clearBasketItemsDiscounts();
        applyPromotions();
    }

    private void clearBasketItemsDiscounts() {
        basketItems.forEach(BasketItem::clearDiscounts);
    }

    private void applyPromotions() {
        promotions.stream()
                .filter(promotion -> promotion.canApplyPromotion(this))
                .forEach(promotion -> promotion.applyPromotion(this));
    }

    private Optional<BasketItem> findBasketItem(Product product) {
        return basketItems.stream()
                .filter(basketItem -> basketItem.getProduct().equals(product))
                .findAny();
    }

    private void checkIfBasketClosed() {
        if (isClosed()) {
            throw new BasketClosedException();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private enum BasketStatus {
        OPEN, CLOSED
    }
}
