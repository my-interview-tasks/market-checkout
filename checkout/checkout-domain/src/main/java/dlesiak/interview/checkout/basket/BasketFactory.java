package dlesiak.interview.checkout.basket;

import dlesiak.interview.checkout.promotion.Promotion;
import dlesiak.interview.checkout.promotion.PromotionRepository;

import java.util.Collection;

public class BasketFactory {

    private final PromotionRepository promotionRepository;

    public BasketFactory(PromotionRepository promotionRepository) {
        this.promotionRepository = promotionRepository;
    }

    public Basket create(CustomerId customerId) {
        Collection<Promotion> promotions = promotionRepository.getPromotions();
        return new Basket(promotions, customerId);
    }
}
