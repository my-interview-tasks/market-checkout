package dlesiak.interview.checkout.promotion.expressions;

import java.util.function.Function;
import java.util.function.Predicate;

public interface QuantityExpression {

    Function<Integer, Predicate<Integer>> quantityMoreThan = first -> second -> first > second;
    Function<Integer, Predicate<Integer>> quantityLessThan = first -> second -> first < second;
    Function<Integer, Predicate<Integer>> quantityEqualTo = first -> first::equals;
    Function<Integer, Predicate<Integer>> quantityEqualOrMore = first -> second -> first >= second;
    Function<Integer, Predicate<Integer>> quantityEqualOrLess = first -> second -> first <= second;


}
