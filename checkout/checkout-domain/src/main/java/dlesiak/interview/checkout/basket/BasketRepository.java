package dlesiak.interview.checkout.basket;

import java.util.Collection;
import java.util.Optional;

public interface BasketRepository {

    void saveBasket(Basket basket);

    Optional<Basket> getOpenedBasket(CustomerId customerId);

    Collection<Basket> getClosedBaskets(CustomerId customerId);
}
