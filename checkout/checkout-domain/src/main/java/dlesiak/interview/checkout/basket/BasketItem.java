package dlesiak.interview.checkout.basket;

import dlesiak.interview.checkout.product.Product;
import dlesiak.interview.checkout.product.ProductCode;
import dlesiak.interview.shared.Money;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import static dlesiak.interview.shared.Validators.require;
import static dlesiak.interview.shared.Validators.requireNonNull;

@EqualsAndHashCode(of = {"product"}, callSuper = false)
@ToString
public final class BasketItem extends Discounted {

    private final Product product;
    private int quantity;

    BasketItem(Product product, int quantity) {
        requireNonNull(product);
        this.product = product;
        newQuantity(quantity);
    }

    public Money total() {
        return totalWithoutDiscount().minus(totalDiscount());
    }

    public Money totalWithoutDiscount() {
        return product.getPrice().times(quantity);
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    boolean isForProduct(ProductCode productCode) {
        return product.getProductCode().equals(productCode);
    }

    BasketItem newQuantity(int quantity) {
        require(quantity > 0, "Quantity has to be > 0");
        this.quantity = quantity;
        return this;
    }
}
