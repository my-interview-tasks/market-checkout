package dlesiak.interview.checkout.basket;


import dlesiak.interview.shared.Money;

import java.util.Collection;

public interface Discountable {

    void addDiscount(Discount discount);

    Collection<Discount> getDiscounts();

    default Money totalDiscount() {
        return getDiscounts()
                .stream()
                .map(Discount::getValue)
                .reduce(Money.ZERO, Money::plus);
    }

}
