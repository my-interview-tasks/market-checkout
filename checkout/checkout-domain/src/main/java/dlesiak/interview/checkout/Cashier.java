package dlesiak.interview.checkout;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.BasketFactory;
import dlesiak.interview.checkout.basket.BasketRepository;
import dlesiak.interview.checkout.basket.CustomerId;
import dlesiak.interview.checkout.basket.exception.BasketAlreadyOpenedException;
import dlesiak.interview.checkout.basket.exception.NoOpenedBasketFoundException;
import dlesiak.interview.checkout.basket.exception.NoProductFoundException;
import dlesiak.interview.checkout.product.Product;
import dlesiak.interview.checkout.product.ProductCode;
import dlesiak.interview.checkout.product.ProductRepository;
import dlesiak.interview.checkout.receipt.Receipt;
import dlesiak.interview.checkout.receipt.ReceiptGenerator;
import dlesiak.interview.checkout.receipt.ReceiptRepository;
import dlesiak.interview.shared.Money;
import dlesiak.interview.shared.event.EventPublisher;

public class Cashier {

    //TODO @TRansaction and @Service/component etc.

    private final BasketFactory basketFactory;
    private final BasketRepository basketRepository;
    private final ReceiptRepository receiptRepository;
    private final ReceiptGenerator receiptGenerator;
    private final ProductRepository productRepository;
    private final EventPublisher eventPublisher;

    public Cashier(BasketRepository basketRepository, ProductRepository productRepository, ReceiptRepository receiptRepository, ReceiptGenerator receiptGenerator, BasketFactory basketFactory, EventPublisher eventPublisher) {
        this.basketRepository = basketRepository;
        this.productRepository = productRepository;
        this.receiptRepository = receiptRepository;
        this.receiptGenerator = receiptGenerator;
        this.basketFactory = basketFactory;
        this.eventPublisher = eventPublisher;
    }

    public void openBasket(String customerId) {
        checkIfOpenBasketExist(customerId);
        Basket basket = basketFactory.create(new CustomerId(customerId));
        basketRepository.saveBasket(basket);
        eventPublisher.publish(basket.getAndClearEvents());
    }

    public Money addOrUpdateBasketItem(String customerId, ProductCode productCode, int quantity) {
        Product product = getProduct(productCode);
        Basket basket = getBasketOrCreate(customerId);
        basket.addProduct(product, quantity);
        basketRepository.saveBasket(basket);
        eventPublisher.publish(basket.getAndClearEvents());
        return basket.total();
    }

    public Money removeBasketItem(String customerId, ProductCode productCode) {
        Basket basket = getBasket(customerId);
        basket.removeProduct(productCode);
        basketRepository.saveBasket(basket);
        eventPublisher.publish(basket.getAndClearEvents());
        return basket.total();
    }

    public Receipt checkout(String customerId) {
        //todo product quantity verification
        Basket basket = getBasket(customerId);
        basket.checkout();
        Receipt receipt = receiptGenerator.generate(basket);
        basketRepository.saveBasket(basket);
        receiptRepository.save(receipt);
        eventPublisher.publish(basket.getAndClearEvents());
        return receipt;
    }

    private Product getProduct(ProductCode productCode) {
        return productRepository
                .getProduct(productCode)
                .orElseThrow(NoProductFoundException::new);
    }

    private Basket getBasketOrCreate(String customerId) {
        return basketRepository
                .getOpenedBasket(new CustomerId(customerId))
                .orElseGet(() -> newBasket(customerId));
    }

    private Basket newBasket(String customerId) {
        Basket basket = basketFactory.create(new CustomerId(customerId));
        basketRepository.saveBasket(basket);
        return basket;
    }

    private Basket getBasket(String customerId) {
        return basketRepository
                .getOpenedBasket(new CustomerId(customerId))
                .orElseThrow(NoOpenedBasketFoundException::new);
    }

    private void checkIfOpenBasketExist(String customerId) {
        basketRepository.getOpenedBasket(new CustomerId(customerId))
                .ifPresent(basket -> {
                    throw new BasketAlreadyOpenedException();
                });
    }
}
