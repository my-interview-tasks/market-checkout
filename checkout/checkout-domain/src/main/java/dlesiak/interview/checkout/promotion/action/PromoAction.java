package dlesiak.interview.checkout.promotion.action;

import dlesiak.interview.checkout.basket.Basket;

public interface PromoAction {

    void apply(Basket basket);
}
