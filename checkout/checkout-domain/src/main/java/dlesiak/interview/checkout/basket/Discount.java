package dlesiak.interview.checkout.basket;


import dlesiak.interview.shared.Money;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public final class Discount {
    private final Money value;
}
