package dlesiak.interview.checkout.promotion.expressions;

import dlesiak.interview.checkout.basket.BasketItem;
import dlesiak.interview.checkout.product.Product;
import dlesiak.interview.shared.Money;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public interface BasketItemExpression {

    BiFunction<Integer, Function<Integer, Predicate<Integer>>, Predicate<BasketItem>> basketItemQuantity =
            (quantity, quantityTest) -> basketItem -> quantityTest.apply(basketItem.getQuantity()).test(quantity);

    BiFunction<Money, Function<Money, Predicate<Money>>, Predicate<BasketItem>> basketItemTotal =
            (total, moneyTest) -> basketItem -> moneyTest.apply(total).test(basketItem.total());

    Function<Predicate<Product>, Predicate<BasketItem>> basketItemHasProduct =
            (productTest) -> basketItem -> productTest.test(basketItem.getProduct());


}
