package dlesiak.interview.checkout.promotion;

import dlesiak.interview.checkout.basket.Basket;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class StackedPromotions extends MultiplePromotions {


    //TODO use composition instead of inheritence? Like StackedPromotionStratedy and PrioritizedPromotion?

    @Override
    public void applyPromotion(Basket basket) {
        promotions.stream()
                .filter(promotion -> promotion.canApplyPromotion(basket))
                .forEach(promotion -> promotion.applyPromotion(basket));
    }

}
