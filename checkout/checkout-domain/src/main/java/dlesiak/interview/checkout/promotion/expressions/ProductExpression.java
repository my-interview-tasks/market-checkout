package dlesiak.interview.checkout.promotion.expressions;

import dlesiak.interview.checkout.product.Product;
import dlesiak.interview.checkout.product.ProductCode;
import dlesiak.interview.shared.Money;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public interface ProductExpression {

    BiFunction<Money, Function<Money, Predicate<Money>>, Predicate<Product>> productPrice =
            (price, moneyTest) -> product -> moneyTest.apply(price).test(product.getPrice());

    Function<ProductCode, Predicate<Product>> productCode =
            (code) -> product -> code.equals(product.getProductCode());

}
