package dlesiak.interview.checkout.product;

import java.util.Optional;

public interface ProductRepository {

    Optional<Product> getProduct(ProductCode productCode);
}
