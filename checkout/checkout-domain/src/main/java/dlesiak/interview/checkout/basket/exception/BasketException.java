package dlesiak.interview.checkout.basket.exception;

import lombok.Getter;

@Getter
public class BasketException extends RuntimeException {

    private final CheckoutError error;

    public BasketException(CheckoutError error) {
        this.error = error;
    }
}
