package dlesiak.interview.checkout.promotion.action;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.BasketItem;
import lombok.experimental.SuperBuilder;

import java.util.function.Predicate;
import java.util.stream.Stream;

@SuperBuilder
public abstract class BasketItemApplicablePromotion implements PromoAction {

    private Predicate<BasketItem> applyTo = basketItem -> true;

    protected Stream<BasketItem> basketItems(Basket basket) {
        return basket.getBasketItems().stream()
                .filter(applyTo);
    }

}
