package dlesiak.interview.checkout.promotion.action;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.BasketItem;
import dlesiak.interview.checkout.basket.Discount;
import dlesiak.interview.shared.Money;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class SetSpecialPricePromoAction extends BasketItemApplicablePromotion {

    private final int bundleSize;
    private final Money specialPrice;

    @Override
    public void apply(Basket basket) {
        basketItems(basket)
                .forEach(basketItem -> basketItem.addDiscount(calculateDiscount(basketItem)));
    }

    private Discount calculateDiscount(BasketItem basketItem) {
        int bundleQuantity = basketItem.getQuantity() / bundleSize;
        Money bundleTotal = specialPrice.times(bundleQuantity);

        int restOfNotBundle = basketItem.getQuantity() - (bundleSize * bundleQuantity);
        Money totalOfNotBundle = basketItem
                .getProduct()
                .getPrice()
                .times(restOfNotBundle);

        Money rawTotal = basketItem
                .totalWithoutDiscount();

        Money discountValue = rawTotal.minus(bundleTotal.plus(totalOfNotBundle));
        return new Discount(discountValue);
    }
}
