package dlesiak.interview.checkout.basket.exception;

import static dlesiak.interview.checkout.basket.exception.CheckoutError.EMPTY_BASKET;

public class EmptyBasketException extends BasketException {
    public EmptyBasketException() {
        super(EMPTY_BASKET);
    }
}
