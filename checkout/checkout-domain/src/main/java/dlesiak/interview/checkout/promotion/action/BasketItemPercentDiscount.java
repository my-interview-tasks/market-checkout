package dlesiak.interview.checkout.promotion.action;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.BasketItem;
import dlesiak.interview.checkout.basket.Discount;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class BasketItemPercentDiscount extends BasketItemApplicablePromotion {

    private final int percent;

    @Override
    public void apply(Basket basket) {
        basketItems(basket)
                .forEach(basketItem -> basketItem.addDiscount(calculateDiscount(basketItem)));
    }

    private Discount calculateDiscount(BasketItem basketItem) {
        return new Discount(basketItem.total().percent(percent));
    }
}
