package dlesiak.interview.checkout.promotion;

import dlesiak.interview.checkout.basket.Basket;

public interface Promotion {

    boolean canApplyPromotion(Basket basket);

    void applyPromotion(Basket basket);

    String name();
}
