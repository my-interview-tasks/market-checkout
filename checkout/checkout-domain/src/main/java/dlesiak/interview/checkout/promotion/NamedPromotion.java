package dlesiak.interview.checkout.promotion;

import lombok.experimental.SuperBuilder;

@SuperBuilder
public abstract class NamedPromotion implements Promotion {

    private final String name;

    @Override
    public String name() {
        return name;
    }
}
