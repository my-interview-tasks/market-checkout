package dlesiak.interview.checkout.promotion;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.promotion.action.PromoAction;
import lombok.experimental.SuperBuilder;

import java.util.function.Predicate;

@SuperBuilder
public class RuleBasedPromotion extends NamedPromotion {

    private final PromoAction promoAction;
    private final Predicate<Basket> applyWhen;

    @Override
    public boolean canApplyPromotion(Basket basket) {
        return applyWhen.test(basket);
    }

    @Override
    public void applyPromotion(Basket basket) {
        promoAction.apply(basket);
    }
}
