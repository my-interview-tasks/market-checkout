package dlesiak.interview.checkout.promotion.action;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.Discount;
import dlesiak.interview.shared.Money;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class WholeBasketFixedDiscount implements PromoAction {

    private final Money discount;

    @Override
    public void apply(Basket basket) {
        basket.addDiscount(new Discount(discount));
    }
}
