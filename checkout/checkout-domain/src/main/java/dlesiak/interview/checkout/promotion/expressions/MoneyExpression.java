package dlesiak.interview.checkout.promotion.expressions;


import dlesiak.interview.shared.Money;

import java.util.function.Function;
import java.util.function.Predicate;

public interface MoneyExpression {

    Function<Money, Predicate<Money>> moneyMoreThan = money -> money::moreThan;
    Function<Money, Predicate<Money>> moneyLessThan = money -> money::lessThan;
    Function<Money, Predicate<Money>> moneyEqualOrLess = money -> money::equalOrLess;
    Function<Money, Predicate<Money>> moneyEqualOrMore = money -> money::equalOrMore;
    Function<Money, Predicate<Money>> moneyEqualTo = money -> money::equals;

}
