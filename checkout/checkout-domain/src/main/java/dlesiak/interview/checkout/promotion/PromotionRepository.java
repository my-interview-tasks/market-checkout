package dlesiak.interview.checkout.promotion;

import java.util.Collection;

public interface PromotionRepository {

    Collection<Promotion> getPromotions();
}
