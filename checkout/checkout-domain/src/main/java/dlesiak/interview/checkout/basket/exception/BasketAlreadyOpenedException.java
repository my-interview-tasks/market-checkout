package dlesiak.interview.checkout.basket.exception;

import static dlesiak.interview.checkout.basket.exception.CheckoutError.BASKET_ALREADY_OPENED;

public class BasketAlreadyOpenedException extends BasketException {
    public BasketAlreadyOpenedException() {
        super(BASKET_ALREADY_OPENED);
    }
}
