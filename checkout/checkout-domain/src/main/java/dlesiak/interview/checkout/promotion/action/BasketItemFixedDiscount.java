package dlesiak.interview.checkout.promotion.action;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.Discount;
import dlesiak.interview.shared.Money;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class BasketItemFixedDiscount extends BasketItemApplicablePromotion {

    private final Money fixedDiscount;

    @Override
    public void apply(Basket basket) {
        basketItems(basket)
                .forEach(basketItem -> basketItem.addDiscount(new Discount(fixedDiscount)));
    }

}
