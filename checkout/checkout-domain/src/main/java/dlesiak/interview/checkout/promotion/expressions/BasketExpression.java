package dlesiak.interview.checkout.promotion.expressions;

import dlesiak.interview.checkout.basket.Basket;
import dlesiak.interview.checkout.basket.BasketItem;
import dlesiak.interview.shared.Money;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public interface BasketExpression {
    Function<Collection<Predicate<BasketItem>>, Predicate<Basket>> foundAllInBasket = tests -> basket -> tests.stream()
            .allMatch(t -> basket.getBasketItems().stream().anyMatch(t));

    Function<Predicate<BasketItem>, Predicate<Basket>> foundInBasket = test -> basket -> basket.getBasketItems()
            .stream().anyMatch(test);

    BiFunction<Money, Function<Money, Predicate<Money>>, Predicate<Basket>> basketTotal =
            (money, moneyTest) -> basket -> moneyTest.apply(money).test(basket.total());

    BiFunction<Integer, Function<Integer, Predicate<Integer>>, Predicate<Basket>> basketQuantity =
            (quantity, quantityTest) -> basket -> quantityTest.apply(quantity).test(basket.size());

}
