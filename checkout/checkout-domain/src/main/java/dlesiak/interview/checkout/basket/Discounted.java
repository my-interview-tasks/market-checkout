package dlesiak.interview.checkout.basket;


import dlesiak.interview.shared.event.EventEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

abstract class Discounted extends EventEntity implements Discountable {

    //TODO USE COMPOSITION instead of inheritence?


    private final Collection<Discount> discounts = new ArrayList<>();

    void clearDiscounts() {
        this.discounts.clear();
    }

    @Override
    public void addDiscount(Discount discount) {
        this.discounts.add(discount);
    }

    @Override
    public Collection<Discount> getDiscounts() {
        return Collections.unmodifiableCollection(discounts);
    }

}
