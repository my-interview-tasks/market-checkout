package dlesiak.interview.checkout.promotion;

import dlesiak.interview.checkout.basket.Basket;
import lombok.Singular;
import lombok.experimental.SuperBuilder;

import java.util.Collection;
import java.util.function.Predicate;

@SuperBuilder
public abstract class MultiplePromotions extends NamedPromotion {

    //TODO use composition instead of inheritence?

    @Singular
    protected final Collection<Promotion> promotions;
    private final Predicate<Basket> applyWhen;

    @Override
    public boolean canApplyPromotion(Basket basket) {
        return applyWhen.test(basket);
    }

}
