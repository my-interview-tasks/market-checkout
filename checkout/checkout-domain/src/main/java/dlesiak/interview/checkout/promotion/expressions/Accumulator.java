package dlesiak.interview.checkout.promotion.expressions;

import java.util.Collection;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public interface Accumulator {

    static <T, R> Predicate<R> mapToMatchAll(Collection<T> collection, Function<T, Predicate<R>> mapper) {
        return collection.stream()
                .map(mapper)
                .reduce(Predicate::and)
                .orElse(a -> false);
    }

    static <T, R> Collection<Predicate<R>> combine(Collection<T> collection, Function<T, Predicate<R>> mapper) {
        return collection.stream()
                .map(mapper)
                .collect(Collectors.toList());
    }

    static <T, R> Predicate<R> anyOf(Collection<T> collection, Function<T, Predicate<R>> mapper) {
        return collection.stream()
                .map(mapper)
                .reduce(Predicate::or)
                .orElse(a -> false);
    }

}
