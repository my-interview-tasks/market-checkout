package dlesiak.interview.checkout.basket.exception;

import static dlesiak.interview.checkout.basket.exception.CheckoutError.BASKET_CLOSED;

public class BasketClosedException extends BasketException {
    public BasketClosedException() {
        super(BASKET_CLOSED);
    }
}
