package dlesiak.interview.checkout.receipt;

import dlesiak.interview.checkout.basket.CustomerId;

import java.util.Optional;

public interface ReceiptRepository {

    void save(Receipt receipt);

    Optional<Receipt> findLastReceipt(CustomerId customerId);
}
