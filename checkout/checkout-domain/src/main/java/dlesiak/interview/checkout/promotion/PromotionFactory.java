package dlesiak.interview.checkout.promotion;

import dlesiak.interview.checkout.product.ProductCode;
import dlesiak.interview.checkout.promotion.action.BasketItemPercentDiscount;
import dlesiak.interview.checkout.promotion.action.SetSpecialPricePromoAction;
import dlesiak.interview.checkout.promotion.action.WholeBasketFixedDiscount;
import dlesiak.interview.shared.Money;

import java.util.Collection;

import static dlesiak.interview.checkout.promotion.expressions.Accumulator.anyOf;
import static dlesiak.interview.checkout.promotion.expressions.Accumulator.combine;
import static dlesiak.interview.checkout.promotion.expressions.BasketExpression.foundAllInBasket;
import static dlesiak.interview.checkout.promotion.expressions.BasketExpression.foundInBasket;
import static dlesiak.interview.checkout.promotion.expressions.BasketItemExpression.basketItemHasProduct;
import static dlesiak.interview.checkout.promotion.expressions.BasketItemExpression.basketItemQuantity;
import static dlesiak.interview.checkout.promotion.expressions.ProductExpression.productCode;
import static dlesiak.interview.checkout.promotion.expressions.QuantityExpression.quantityEqualOrMore;

public class PromotionFactory {

    public Promotion fixedWholeBasketDiscount(String promotionName, Collection<ProductCode> codes, Money discount) {
        return RuleBasedPromotion.builder()
                .name(promotionName)
                .applyWhen(
                        foundAllInBasket.apply(
                                combine(codes, code -> basketItemHasProduct.apply(productCode.apply(code)))))
                .promoAction(new WholeBasketFixedDiscount(discount))
                .build();
    }

    public Promotion bundlePromotion(String name, ProductCode code, int bundleSize, Money price) {
        return RuleBasedPromotion.builder()
                .name(name)
                .applyWhen(
                        foundInBasket.apply(
                                basketItemHasProduct.apply(productCode.apply(code))
                                        .and(basketItemQuantity.apply(bundleSize, quantityEqualOrMore)))
                )
                .promoAction(
                        SetSpecialPricePromoAction.builder()
                                .bundleSize(bundleSize)
                                .specialPrice(price)
                                .applyTo(basketItemHasProduct.apply(productCode.apply(code)))
                                .build())
                .build();
    }

    public Promotion percentDiscountBasketItem(String name, Collection<ProductCode> codes, int percentDiscount) {
        return RuleBasedPromotion.builder()
                .name(name)
                .applyWhen(foundAllInBasket.apply(
                        combine(codes, code -> basketItemHasProduct.apply(productCode.apply(code)))))
                .promoAction(
                        BasketItemPercentDiscount.builder()
                                .applyTo(anyOf(codes, code -> basketItemHasProduct.apply(productCode.apply(code))))
                                .percent(percentDiscount)
                                .build()
                )
                .build();
    }


}
