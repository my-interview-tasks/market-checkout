package dlesiak.interview.checkout

import dlesiak.interview.checkout.product.Product
import dlesiak.interview.checkout.product.ProductCode
import dlesiak.interview.shared.Money

trait ProductFixture {

    static Product COFFEE = new Product(new ProductCode("code-coffee"), "coffee", Money.of(10))
    static Product TEA = new Product(new ProductCode("code-tea"), "tea", Money.of(40))
    static Product VODKA = new Product(new ProductCode("code-vodka"), "vodka", Money.of(200))

}