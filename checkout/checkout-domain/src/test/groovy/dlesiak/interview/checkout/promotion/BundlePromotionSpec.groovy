package dlesiak.interview.checkout.promotion

import dlesiak.interview.checkout.ProductFixture
import dlesiak.interview.checkout.basket.Basket
import dlesiak.interview.checkout.basket.CustomerId
import dlesiak.interview.shared.Money
import spock.lang.*

@Stepwise
@Title("Buy N of products and pay Y money (bundle promotion)")
class BundlePromotionSpec extends Specification implements ProductFixture {

    @Subject
    @Shared
    Basket basket

    @Shared
    def customerId = new CustomerId("JanNowak")

    def setupSpec() {
        def promotion = new PromotionFactory().bundlePromotion(
                "Buy 3 x Tea and pay 70",
                TEA.productCode,
                3,
                Money.of(70))
        basket = new Basket([promotion], customerId)
    }

    def "customer new open basket"() {
        when: "new open basket"

        then: "basket total before discount is 0"
        basket.totalWithoutDiscount() == Money.ZERO
        basket.total() == Money.ZERO
    }

    def "customer buy product not in promotion"() {
        when: "customer adds 1 x VODKA to basket"
        basket.addProduct(VODKA, 1)

        then: "basket total is VODKA total"
        basket.totalWithoutDiscount() == VODKA.price
        basket.total() == VODKA.price
    }

    def "customer removes VODKA"() {
        when:
        basket.removeProduct(VODKA.productCode)

        then:
        basket.totalWithoutDiscount() == Money.ZERO
    }

    def "customer buy less than bundle size promotion required"() {
        when: "customer adds 2 x TEA to basket"
        basket.addProduct(TEA, 2)

        then: "promotion is not applied"
        basket.totalWithoutDiscount() == TEA.price.times(2)
        basket.total() == TEA.price.times(2)
    }

    def "customer buy required quantity of bundle size promotion"() {
        when: "customer update product quantity to 3 x TEA"
        basket.addProduct(TEA, 3)

        then: "promotion is applied"
        basket.total() == Money.of(70)
        basket.totalWithoutDiscount() == TEA.price.times(3)
    }

    def "customer buy more than bundle promotion size"() {
        when: "customer update product quantity to 5 x TEA"
        basket.addProduct(TEA, 5)

        then: "promotion is applied for 3 x TEA and  other 2 x TEA for regular price"
        basket.total() == Money.of(70) + TEA.price.times(2)
        basket.totalWithoutDiscount() == TEA.price.times(5)
    }

    def "customer buy more than 2 bundle promotion size"() {
        when: "customer update product quantity to 8 tea"
        basket.addProduct(TEA, 8)

        then: "promotion is applied for 2 set of TEA (6 x TEA) and other 2 x TEA for regular price"
        basket.total() == Money.of(70).times(2) + TEA.price.times(2)
        basket.totalWithoutDiscount() == TEA.price.times(8)
    }

    def "customer add other product not in promotion"() {
        when: "customer adds 2 x COFFEE"
        basket.addProduct(COFFEE, 2)

        then: "adding other product does not affect promotion"
        basket.total() ==
                COFFEE.price.times(2) +
                Money.of(70).times(2) +
                TEA.price.times(2)

        basket.totalWithoutDiscount() ==
                TEA.price.times(8) +
                COFFEE.price.times(2)
    }

    def "customer resign from promotion product"() {
        when: "customer remove TEA"
        basket.removeProduct(TEA.productCode)

        then: "promotion is not applied"
        basket.total() == basket.totalWithoutDiscount()
    }

}