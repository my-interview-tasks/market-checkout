package dlesiak.interview.checkout

import dlesiak.interview.checkout.basket.Basket
import dlesiak.interview.checkout.basket.BasketFactory
import dlesiak.interview.checkout.basket.BasketRepository
import dlesiak.interview.checkout.basket.CustomerId
import dlesiak.interview.checkout.basket.exception.BasketAlreadyOpenedException
import dlesiak.interview.checkout.basket.exception.NoOpenedBasketFoundException
import dlesiak.interview.checkout.basket.exception.NoProductFoundException
import dlesiak.interview.checkout.product.Product
import dlesiak.interview.checkout.product.ProductCode
import dlesiak.interview.checkout.product.ProductRepository
import dlesiak.interview.checkout.receipt.ReceiptGenerator
import dlesiak.interview.checkout.receipt.ReceiptRepository
import dlesiak.interview.shared.Money
import dlesiak.interview.shared.event.EventPublisher
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class CashierSpec extends Specification implements ProductFixture {

    BasketRepository basketRepository = Mock()
    ProductRepository productRepository = Mock()
    ReceiptRepository receiptRepository = Mock()
    BasketFactory basketFactory = Mock()
    EventPublisher eventPublisher = Mock()

    @Subject
    Cashier cashier = new Cashier(
            basketRepository,
            productRepository,
            receiptRepository,
            new ReceiptGenerator(),
            basketFactory,
            eventPublisher)

    @Shared
    String customerId = "JanNowak"

    def "open new basket when adding first product"() {
        given: "product id"
        def code = new ProductCode("coffee")

        and: "quantity"
        def quantity = 2

        and: "coffee product available"
        coffeeInDb(code)

        and: "no basket opened basket"
        basketRepository.getOpenedBasket(_ as CustomerId) >> Optional.empty()

        when: "adding first product to basket"
        cashier.addOrUpdateBasketItem(customerId, code, quantity)

        then: "new basket is created"
        1 * basketFactory.create(_ as CustomerId) >> new Basket(new CustomerId("JanKowalski"))

        //TODO test that specific events are published, not by size
        and: "BasketOpened event is published"
        and: "ItemAddedToBasket event is published"
        1 * eventPublisher.publish({ it.size() == 2 }) //todo
    }

    def "do not create new basket if one already exist"() {
        given: "product id"
        def code = new ProductCode("coffee")

        and: "quantity"
        def quantity = 2

        and: "basket is already open"
        basketRepository.getOpenedBasket(_ as CustomerId) >> Optional.of(basket())

        and: "coffee product available"
        coffeeInDb(code)

        when: "adding product to basket"
        cashier.addOrUpdateBasketItem(customerId, code, quantity)

        then: "no another basket is created"
        0 * basketFactory.create(_ as CustomerId)

        //TODO test that specific events are published, not by size
        and: "ItemAddedToBasket is published only"
        1 * eventPublisher.publish({ it.size() == 1 })
    }

    def "throw error when adding not available product to basket"() {
        given: "product not available"
        productRepository.getProduct(new ProductCode("coffee")) >> Optional.empty()

        when: "adding product"
        cashier.addOrUpdateBasketItem(customerId, new ProductCode("coffee"), 4)

        then: "throw error"
        NoProductFoundException ex = thrown()

        and: "no events are published"
        0 * eventPublisher.publish(_)
    }


    def "remove product from basket"() {
        given: "basket to checkout"
        basketRepository.getOpenedBasket(_) >> Optional.of(basket())

        when: "remove item"
        cashier.removeBasketItem(customerId, TEA.productCode)

        then: "basket is saved"
        1 * basketRepository.saveBasket(_)

        //TODO test that specific events are published, not by size
        and: "ItemRemovedFromBasket is published"
        1 * eventPublisher.publish({ it.size() == 1 })
    }


    def "throw error if no basket when removing product"() {
        given: "no open basket"
        basketRepository.getOpenedBasket(_) >> Optional.empty()

        when: "checking out"
        cashier.removeBasketItem(customerId, new ProductCode("coffee"))

        then: "throw error"
        NoOpenedBasketFoundException ex = thrown()

        and: "no events are published"
        0 * eventPublisher.publish(_)
    }

    def "checkout basket"() {
        given: "basket to checkout"
        basketRepository.getOpenedBasket(_) >> Optional.of(basket())

        when: "checking out"
        cashier.checkout(customerId)

        then: "basket is saved"
        1 * basketRepository.saveBasket(_)

        and: "receipt is saved"
        1 * receiptRepository.save(_)

        //TODO test that specific events are published, not by size
        and: "BasketClosed event is published"
        1 * eventPublisher.publish({ it.size() == 1 })
    }

    def "throw error if no basket when checking out"() {
        given: "no open basket"
        basketRepository.getOpenedBasket(_) >> Optional.empty()

        when: "checking out"
        cashier.checkout(customerId)

        then: "throw error"
        NoOpenedBasketFoundException ex = thrown()

        and: "no events are published"
        0 * eventPublisher.publish(_)
    }

    def "throw error if opening new basket when basket is already opened"() {
        given: "already open basket"
        basketRepository.getOpenedBasket(_) >> Optional.of(basket())

        when: "opens new basket"
        cashier.openBasket(customerId)

        then: "throw error"
        BasketAlreadyOpenedException ex = thrown()

        and: "no events are published"
        0 * eventPublisher.publish(_)
    }

    def coffeeInDb(productId) {
        productRepository.getProduct(productId) >>
                Optional.of(new Product(
                        productId,
                        "product",
                        Money.of(25)))
    }

    private Basket basket() {
        def basket = new Basket(new CustomerId(customerId))
        basket.addProduct(TEA, 2)
        basket.getAndClearEvents()
        basket
    }

}
