package dlesiak.interview.checkout.basket

import dlesiak.interview.checkout.ProductFixture
import dlesiak.interview.checkout.basket.event.BasketClosed
import dlesiak.interview.checkout.basket.event.BasketOpened
import dlesiak.interview.checkout.basket.event.ItemAddedToBasket
import dlesiak.interview.checkout.basket.event.ItemRemovedFromBasket
import dlesiak.interview.checkout.basket.exception.BasketClosedException
import dlesiak.interview.checkout.basket.exception.EmptyBasketException
import dlesiak.interview.shared.event.Event
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.lang.Subject

@Stepwise
class BasketSpec extends Specification implements ProductFixture {

    @Shared
    CustomerId customerId = new CustomerId("CustomerId")
    @Shared
    @Subject
    Basket basket

    def "open new empty basket"() {
        when: "create basket"
        basket = new Basket(customerId)

        then: "basket is open and ready for adding products"
        basket.isOpen()

        and: "BasketOpened event is created"
        basket.getEvents().size() == 1
        hasEvents(basket, BasketOpened.class).size() == 1
    }

    def "cannot checkout empty basket"() {
        when: "try to checkout"
        basket.checkout()

        then: "error is thrown"
        thrown(EmptyBasketException.class)
    }

    def "add product to new empty basket"() {
        when: "add 2 x TEA to basket"
        basket.addProduct(TEA, 2)

        then: "basket is updated"
        basket.size() == 1
        basket.total() == TEA.price.times(2)
        basket.getBasketItems().find { it.product == TEA && it.quantity == 2 }

        and: "ItemAddedToBasket event is created"
        basket.getEvents().size() == 2
        hasEvents(basket, ItemAddedToBasket.class).size() == 1
    }

    def "update quantity of product in basket"() {
        when: "update TEA quantity to 4"
        basket.addProduct(TEA, 4)

        then: "basket items size does not change"
        basket.size() == 1

        and: "product quantity is updated"
        basket.getBasketItems().find { it.product == TEA && it.quantity == 4 }

        and: "total is updated"
        basket.total() == TEA.price.times(4)

        and: "ItemAddedToBasket event is created"
        basket.getEvents().size() == 3
        hasEvents(basket, ItemAddedToBasket.class).size() == 2
    }

    def "add new products to basket"() {
        when: "add VODKA to basket"
        basket.addProduct(VODKA, 10)

        then: "basket size is updated"
        basket.size() == 2

        and: "basket has TEA"
        basket.getBasketItems().find { it.product == TEA && it.quantity == 4 }

        and: "basket has 10 x VODKA"
        basket.getBasketItems().find { it.product == VODKA && it.quantity == 10 }

        and: "total is updated"
        basket.total() == TEA.price.times(4) + VODKA.price.times(10)
    }

    def "remove product from basket"() {
        when: "remove TEA from basket"
        basket.removeProduct(TEA.productCode)

        then: "basket size is decreased"
        basket.size() == 1

        and: "no TEA in basket"
        !basket.getBasketItems().find { it.product == TEA }

        and: "total is decreased to VODKA total"
        basket.total() == VODKA.price.times(10)

        and: "ItemRemovedFromBasket event is created"
        hasEvents(basket, ItemRemovedFromBasket.class).size() == 1
    }

    def "nothing happened if removing product not in basket"() {
        when: "remove COFFEE from basket"
        basket.removeProduct(COFFEE.productCode)

        then: "basket size not changed"
        basket.size() == 1

        and: "only VODKA in basket"
        basket.getBasketItems().find { it.product == VODKA }

        and: "total not changed"
        basket.total() == VODKA.getPrice().times(10)

        and: "ItemRemovedFromBasket event quantity is not changed"
        hasEvents(basket, ItemRemovedFromBasket.class).size() == 1
    }

    def "checkout basket and return total"() {
        when: "checkout"
        basket.checkout()

        then: "basket total is equal  to 10 x VODKA"
        basket.total() == VODKA.getPrice().times(10)

        and: "basket is closed"
        basket.isClosed()

        and: "BasketClosed event is created"
        hasEvents(basket, BasketClosed.class).size() == 1
    }

    def "cannot remove products from closed basket"() {
        when: "try to remove product"
        basket.removeProduct(VODKA.productCode)

        then: "error is thrown"
        thrown(BasketClosedException.class)
    }

    def "cannot add products to closed basket"() {
        when: "try to add product"
        basket.addProduct(TEA, 2)

        then: "error is thrown"
        thrown(BasketClosedException.class)
    }

    def "cannot checkout closed basket"() {
        when: "try to checkout"
        basket.checkout()

        then: "error is thrown"
        thrown(BasketClosedException.class)
    }

    private static List<Event> hasEvents(Basket basket, Class<Event> clazz) {
        basket.getEvents().findAll { it.getClass() == clazz }
    }
}
