package dlesiak.interview.checkout.promotion

import dlesiak.interview.checkout.ProductFixture
import dlesiak.interview.checkout.basket.Basket
import dlesiak.interview.checkout.basket.CustomerId
import dlesiak.interview.shared.Money
import spock.lang.*

@Stepwise
@Title("Buy X and Y and get Z fixed discount for whole basket")
class BuyXAndYGetFixedDiscountAppliedToBasketSpec extends Specification implements ProductFixture {

    @Shared
    @Subject
    Basket basket

    @Shared
    def customerId = new CustomerId("JanNowak")

    @Shared
    def discount = Money.of(10)

    def setupSpec() {
        def promotion = new PromotionFactory()
                .fixedWholeBasketDiscount(
                        "Buy TEA and COFFEE and get 10 discount",
                        Arrays.asList(TEA.productCode, COFFEE.productCode),
                        Money.of(10))
        basket = new Basket([promotion], customerId)
    }

    def "customer buy products not applied to promotion"() {
        when: "customer adds TEA and VODKA to basket"
        basket.addProduct(TEA, 1)
        basket.addProduct(VODKA, 1)

        then: "promotion is not applied"
        basket.total() == TEA.price + VODKA.price
    }

    def "customer buy product that trigger promotion"() {
        when: "customer adds COFFEE to basket"
        basket.addProduct(COFFEE, 1)

        then: "promotion is applied"
        basket.total() == (TEA.price + VODKA.price + COFFEE.price) - discount
    }

    def "customer resign from promotion product"() {
        when: "customer removes TEA from basket"
        basket.removeProduct(TEA.productCode)

        then: "promotion is not applied"
        basket.total() == (VODKA.price + COFFEE.price)
    }
}
