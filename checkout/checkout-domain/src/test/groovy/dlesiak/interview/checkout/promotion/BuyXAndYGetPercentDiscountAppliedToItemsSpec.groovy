package dlesiak.interview.checkout.promotion

import dlesiak.interview.checkout.ProductFixture
import dlesiak.interview.checkout.basket.Basket
import dlesiak.interview.checkout.basket.CustomerId
import spock.lang.*

@Stepwise
@Title("Buy X and Y and get Z% discount for X and Y")
class BuyXAndYGetPercentDiscountAppliedToItemsSpec extends Specification implements ProductFixture {

    @Shared
    def customerId = new CustomerId("JanNowak")

    @Shared
    def discount = 20

    @Shared
    @Subject
    Basket basket

    def setupSpec() {
        Promotion promotion = new PromotionFactory().percentDiscountBasketItem(
                "Buy VODKA and TEA and get 20% discount",
                Arrays.asList(VODKA.productCode, TEA.productCode),
                20
        )
        basket = new Basket([promotion], customerId)
    }

    def "customer buy products not applied to promotion"() {
        when: "customer adds TEA and COFFEE to basket"
        basket.addProduct(TEA, 1)
        basket.addProduct(COFFEE, 2)

        then: "promotion is not applied"
        basket.total() == TEA.price + COFFEE.price.times(2)
    }

    def "customer buy product that trigger promotion"() {
        when: "customer adds VODKA to basket"
        basket.addProduct(VODKA, 2)

        then: "promotion is applied"
        basket.total() ==
                COFFEE.price.times(2) +
                (TEA.price - TEA.price.percent(discount)) +
                (VODKA.price.times(2) - VODKA.price.times(2).percent(discount))
    }

    def "customer resign from promotion product"() {
        when: "customer removes VODKA from basket"
        basket.removeProduct(VODKA.productCode)

        then: "promotion is not applied"
        basket.total() == basket.totalWithoutDiscount()
    }
}
