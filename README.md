
## How to run 

You can run it by Docker:

mvn clean install && docker build -t checkout . && docker-compose up --build

host: localhost:8085

## Task description

Task description is in pdf file "Senior Java Developer Recruitment Task.pdf" and also  "Task - Junior Java Developer position.pdf". 
All features from Senior Java Task and Junior Java Task are in this project.

## API

List of closed baskets:

```
GET http://localhost:8085/basket?customerId=marek&status=CLOSED

```

Current basket:
```
GET http://localhost:8085/basket?customerId=marek&status=OPENED
```

List of products:
```
GET http://localhost:8085/products
```

Open new basket:
```
PUT http://localhost:8085/basket?customerId=marek
```

Add or update product to basket (and open new basket if not exist):
Quantity is replaced not added to exist quantity.
```
POST http://localhost:8085/basket/item
request = [
                productId : 'coffee',
                quantity  : 2,
                customerId: 'janNowak'
        ]
```

Remove product from basket:
```
DELETE http://localhost:8085/basket/item?productId=coffee&customerId=jannowak
```

Checkout basket:
```
POST http://localhost:8085/basket/checkout?customerId=marek
```

## Notes

0. This is interview task, so I didnt spend too much time in polishing code. There are TODOs saying what has to be done and what can be done better.

1. Promotions are build using Java lambdas/functions, which allows to build complex rules without refactoring code like extending classes etc.
 
For example:

```java
 RuleBasedPromotion.builder()
                .name(name)
                .applyWhen(
                        foundInBasket.apply(
                                basketItemHasProduct.apply(productCode.apply(code))
                                        .and(basketItemQuantity.apply(bundleSize, quantityEqualOrMore)))
                )
                .promoAction(
                        SetSpecialPricePromoAction.builder()
                                .bundleSize(bundleSize)
                                .specialPrice(price)
                                .applyTo(basketItemHasProduct.apply(productCode.apply(code)))
                                .build())
                .build();
```

You can write promotion rules without extending or implementing features. Just combine promotion rules (lambdas).
//TODO checkout VAVR library

2. There are two bounded-context within separate maven modules: checkout and product-catalogue.
   Additionally, checkout module is break down to api, infrastructure and domain module. (For small project like this, it is overkill. Using packages as structure would work as well but using maven modules, you dont couple domain with api/infra dependencies)
    
3. It is singe monolith app, but can be break down to microservices, as checkout and product-catalogue microservices

4. Product catalogue and receipt are stored in postgresql.

5. Baskets and promotions are stored in memory.

6. Stack: 
* Java 8
* Spring Boot
* Spock/Groovy (decided to use Spock because  of it`s data-driven test features. Also, in my opinion, tests written in Spock are easier to write and read than junit tests)
* Lombok 

7. Acceptance test are written using TestContainers library.









