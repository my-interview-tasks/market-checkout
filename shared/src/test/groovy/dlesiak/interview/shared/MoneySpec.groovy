package dlesiak.interview.shared

import spock.lang.Specification

class MoneySpec extends Specification {

    def "add two money"() {
        given: "five"
        Money five = Money.of(5)

        and: "eight"
        Money eight = Money.of(8)

        when: "adding"
        Money money = five.plus(eight)

        then: "it is 13"
        money == Money.of(13)
    }

    def "subtract money"() {
        given: "five"
        Money five = Money.of(5)

        and: "eight"
        Money eight = Money.of(8)

        when: "subtract"
        Money money = eight.minus(five)

        then: "it is 3" //todo money can be negative?
        money == Money.of(3)
    }

    def "multiply money"() {
        given: "eight"
        Money eight = Money.of(8)

        when: "multiply"
        Money money = eight.times(5)

        then: "it is 40"
        money == Money.of(40)
    }

    def "sum money"() {
        given: "eight"
        Money eight = Money.of(8)

        and: "five"
        Money five = Money.of(5)

        and: "hundred"
        Money hundred = Money.of(100)

        when: "sum"
        Money money = Money.sum(eight, five, hundred)

        then: "sum is 113"
        money == Money.of(113)
    }

    def "percent"() {
        given: "money"
        def money = Money.of(70)

        expect:
        Money.of(7) == money.percent(10)
    }

    def "more than"() {
        given:
        Money money = Money.of(first)

        and:
        Money other = Money.of(second)

        expect:
        moreThan == money.moreThan(other)

        where:
        first | second | moreThan
        1     | 2      | false
        1     | 24     | false
        1     | -2     | true
        -2    | 2      | false
        11    | 2      | true
        0     | 0      | false
    }

    def "equal or more than"() {
        given:
        Money money = Money.of(first)

        and:
        Money other = Money.of(second)

        expect:
        moreThan == money.equalOrMore(other)

        where:
        first | second | moreThan
        1     | 2      | false
        1     | 24     | false
        1     | -2     | true
        -2    | 2      | false
        11    | 2      | true
        0     | 0      | true
    }


    def "less than"() {
        given:
        Money money = Money.of(first)

        and:
        Money other = Money.of(second)

        expect:
        lessThan == money.lessThan(other)

        where:
        first | second | lessThan
        12    | 2      | false
        145   | 24     | false
        1     | -2     | false
        -1    | 2      | true
        11    | 22     | true
        0     | 0      | false
    }

    def "equal or less than"() {
        given:
        Money money = Money.of(first)

        and:
        Money other = Money.of(second)

        expect:
        lessThan == money.equalOrLess(other)

        where:
        first | second | lessThan
        12    | 2      | false
        145   | 24     | false
        1     | -2     | false
        -1    | 2      | true
        11    | 22     | true
        0     | 0      | true
    }

    //TODO test equals contract
    def "money equal"() {
        given:
        Money money = Money.of(5)

        and:
        Money other = Money.of(5)

        expect:
        money == other
    }

    def "money does not equal"() {
        given:
        Money money = Money.of(5)

        and:
        Money other = Money.of(25)

        expect:
        money != other
    }
}
