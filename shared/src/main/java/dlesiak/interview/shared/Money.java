package dlesiak.interview.shared;

import lombok.ToString;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;

@ToString
public final class Money {

    public static final Money ZERO = Money.of(0);

    private final BigDecimal value;

    private Money(int value) {
        this.value = BigDecimal.valueOf(value);
    }

    private Money(BigDecimal value) {
        this.value = value;
    }

    public static Money sum(Money... moneys) {
        return Arrays.stream(moneys)
                .reduce(new Money(0), Money::plus);
    }

    public static Money of(int value) {
        return new Money(value);
    }

    public Money plus(Money money) {
        return new Money(this.value.add(money.value));
    }

    public Money minus(Money money) {
        return new Money(this.value.subtract(money.value));
    }

    public Money times(double multiplier) {
        return new Money(this.value.multiply(BigDecimal.valueOf(multiplier)));
    }

    public Money percent(int percent) {
        return this.times(percent / 100.0);
    }

    public boolean lessThan(Money money) {
        return this.value.compareTo(money.value) < 0;
    }

    public boolean moreThan(Money other) {
        return value.compareTo(other.value) > 0;
    }

    public boolean equalOrLess(Money money) {
        return this.value.compareTo(money.value) <= 0;
    }

    public boolean equalOrMore(Money money) {
        return this.value.compareTo(money.value) >= 0;
    }

    public Integer toInteger() {
        return value.intValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Money money = (Money) o;
        return Objects.equals(value.stripTrailingZeros(), money.value.stripTrailingZeros());
    }

    @Override
    public int hashCode() {
        return Objects.hash(value.stripTrailingZeros());
    }
}
