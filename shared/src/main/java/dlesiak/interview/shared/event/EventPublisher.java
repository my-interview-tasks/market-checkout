package dlesiak.interview.shared.event;

import java.util.Collection;

public interface EventPublisher {

    void publish(Collection<Event> events);
    //TODO implement publishing events, via spring, memory bus, via kafka, jms etc...
    //TODO internal events, external events, depends on bounded context map

}
