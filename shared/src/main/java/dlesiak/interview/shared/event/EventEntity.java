package dlesiak.interview.shared.event;

import dlesiak.interview.shared.Entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public abstract class EventEntity implements Entity {

    private Collection<Event> eventStore = new ArrayList<>();

    protected void addEvent(Event event) {
        eventStore.add(event);
    }

    public Collection<Event> getAndClearEvents() {
        Collection<Event> events = Collections.unmodifiableCollection(eventStore);
        eventStore = new ArrayList<>();
        return events;
    }

    public Collection<Event> getEvents() {
        return Collections.unmodifiableCollection(eventStore);
    }
}
