package dlesiak.interview.product

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.jdbc.Sql
import spock.lang.Specification

@DataJpaTest(showSql = true)
@Sql(scripts = ["/product-test-init.sql"])
@ContextConfiguration(classes = [Config])
class CatalogueProductRepositorySpec extends Specification {

    @Autowired
    private CatalogueProductRepository productRepository

    def "read all products from repository"() {
        when: "reading products"
        def all = productRepository.findBy()

        then: "found all 4 products"
        all.size() == 4
        all.find { it.code == "product-vodka" }
        all.find { it.code == "product-cola" }
        all.find { it.code == "product-tea" }
        all.find { it.code == "product-coffee" }
    }

    def "read products from repository by codes"() {
        given: "product codes"
        def codes = ["product-vodka", "product-cola"]

        when: "reading products"
        def all = productRepository.findAllByCodeIn(codes)

        then: "found 2 products"
        all.size() == 2
        all.find { it.code == "product-vodka" }
        all.find { it.code == "product-cola" }
    }

    def "read products from repository with empty code list"() {
        given: "empty product code list"
        def codes = []

        when: "reading products"
        def all = productRepository.findAllByCodeIn(codes)

        then: "found 0 products"
        all.size() == 0
    }

    def "read products from repository by single code"() {

        when: "reading product"
        def product = productRepository.findByCode("product-vodka")

        then: "found product"
        product.isPresent()
        product.get().with {
            it.name == 'VODKA'
            it.code == 'product-vodka'
            it.price == 30
            it.description == 'awesome vodka'
        }
    }

    def "return empty for non existent product"() {
        given: "non existent product code"
        def product = "product-xxxx"

        when: "reading product"
        def all = productRepository.findByCode(product)

        then: "found no product"
        !all.isPresent()
    }

    @EntityScan(basePackages = ["dlesiak.interview.product"])
    @EnableJpaRepositories(basePackages = ["dlesiak.interview.product"])
    static class Config {

    }

}
