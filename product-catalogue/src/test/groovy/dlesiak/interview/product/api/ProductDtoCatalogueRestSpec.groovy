package dlesiak.interview.product.api

import com.fasterxml.jackson.databind.ObjectMapper
import dlesiak.interview.product.ProductCatalogue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.mock.DetachedMockFactory

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get


@WebMvcTest
@ContextConfiguration(classes = Config)
class ProductDtoCatalogueRestSpec extends Specification implements ProductDtoFixture {

    @Autowired
    private MockMvc mvc

    @Autowired
    private ProductCatalogue productCatalogue

    @Autowired
    private ObjectMapper objectMapper

    def "return all products when no codes are passed as parameters"() {
        given: "no request parameter"

        when: "query for products"
        def response = mvc.perform(get('/products'))
                .andReturn()
                .response

        then: "response is ok"
        response.status == HttpStatus.OK.value()

        and: "returned all products"
        with(objectMapper.readValue(response.contentAsString, List)) {
            it.size() == 3
        }

        and:
        1 * productCatalogue.findAll() >> [TEA, COFFEE, VODKA]
        0 * productCatalogue.findByCodes(_)
    }

    def "return specific products when codes parameter is passed"() {
        given: "codes parameter"
        String[] codes = ['product-tea', 'product-vodka']

        when: "query for products"
        def response = mvc.perform(get('/products')
                .param('codes', codes))
                .andReturn().response

        then: "response is ok"
        response.status == HttpStatus.OK.value()

        and: "products are returned"
        with(objectMapper.readValue(response.contentAsString, List)) {
            it.size() == 2
        }

        and:
        1 * productCatalogue.findByCodes(_) >> [TEA, VODKA]
        0 * productCatalogue.findAll()
    }

    def "return empty list if no products in catalogue"() {
        given: "no products in catalogue"
        productCatalogue.findAll() >> []

        when: "query for products"
        def response = mvc.perform(get('/products'))
                .andReturn()
                .response

        then: "response is ok"
        response.status == HttpStatus.OK.value()

        and: "returned product list"
        with(objectMapper.readValue(response.contentAsString, List)) {
            it.size() == 0
        }
    }


    def "return single product by code"() {
        given: "product code"
        def code = 'product-tea'

        and: "all products returned"
        productCatalogue.findByCode(code) >> Optional.of(TEA)

        when: "query for products"
        def response = mvc.perform(get('/product')
                .param("code", code))
                .andReturn().response

        then: "response is ok"
        response.status == HttpStatus.OK.value()

        and: "returned product list"
        with(objectMapper.readValue(response.contentAsString, Map)) {
            it.code == 'product-tea'
            it.name == 'Tea'
            it.description == 'best tea'
            it.price == 20
        }
    }

    def "return 404 when product not found"() {
        given: "product code"
        def code = "product-tea"

        and: "all products returned"
        productCatalogue.findByCode(code) >> Optional.empty()

        when: "query for products"
        def response = mvc.perform(get('/product')
                .param("code", code))
                .andReturn().response

        then: "response is 404 NOT FOUND"
        response.status == HttpStatus.NOT_FOUND.value()
    }

    @ComponentScan(basePackageClasses = [ProductController])
    static class Config {
        DetachedMockFactory detachedMockFactory = new DetachedMockFactory()

        @Bean
        ProductCatalogue productCatalogue() {
            return detachedMockFactory.Mock(ProductCatalogue)
        }
    }
}


