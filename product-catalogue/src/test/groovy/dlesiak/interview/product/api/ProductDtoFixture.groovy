package dlesiak.interview.product.api

import dlesiak.interview.product.ProductDto


trait ProductDtoFixture {

    static ProductDto COFFEE = new ProductDto("product-coffee", "Coffee", "best coffee", 10)
    static ProductDto TEA = new ProductDto("product-tea", "Tea", "best tea", 20)
    static ProductDto VODKA = new ProductDto("product-vodka", "Vodka", "best vodka", 200)

}