insert into product (ID, CODE,NAME, DESCRIPTION, PRICE) values(1, 'product-coffee', 'COFFEE','awesome coffee', 10);
insert into product (ID, CODE,NAME, DESCRIPTION, PRICE) values(2, 'product-tea', 'TEA','awesome tea', 40);
insert into product (ID, CODE,NAME, DESCRIPTION, PRICE) values(3, 'product-cola', 'COLA','awesome cola', 25);
insert into product (ID, CODE,NAME, DESCRIPTION, PRICE) values(4, 'product-vodka', 'VODKA','awesome vodka', 30);
