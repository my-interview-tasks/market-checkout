package dlesiak.interview.product;

import org.springframework.data.repository.Repository;

import java.util.Collection;
import java.util.Optional;

interface CatalogueProductRepository extends Repository<Product, Long> {

    Collection<ProductDto> findAllByCodeIn(Collection<String> productCode);

    Optional<ProductDto> findByCode(String productCode);

    Collection<ProductDto> findBy();
}
