package dlesiak.interview.product.api;

import dlesiak.interview.product.ProductCatalogue;
import dlesiak.interview.product.ProductDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Collection;

@RestController
@AllArgsConstructor
public class ProductController {

    private final ProductCatalogue productCatalogue;

    @GetMapping("/products")
    Collection<ProductDto> getAllProducts(@RequestParam(name = "codes", required = false) Collection<String> codes) {
        if (codes == null || codes.isEmpty()) {
            return productCatalogue.findAll();
        } else {
            return productCatalogue.findByCodes(codes);
        }
    }

    @GetMapping("/product")
    ResponseEntity<ProductDto> getProduct(@RequestParam(name = "code") @Valid @NotBlank String code) {
        return productCatalogue.findByCode(code)
                .map(product -> ResponseEntity.ok().body(product))
                .orElse(ResponseEntity.notFound().build());
    }

}
