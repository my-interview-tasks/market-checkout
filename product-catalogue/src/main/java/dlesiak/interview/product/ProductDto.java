package dlesiak.interview.product;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@EqualsAndHashCode(of = "code")
@ToString
public class ProductDto {
    private String code;
    private String name;
    private String description;
    private int price;
}
