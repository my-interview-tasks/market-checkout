package dlesiak.interview.product;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductCatalogue {

    private final CatalogueProductRepository catalogueProductRepository;

    public Optional<ProductDto> findByCode(String code) {
        return catalogueProductRepository.findByCode(code);
    }

    public Collection<ProductDto> findByCodes(Collection<String> codes) {
        return catalogueProductRepository.findAllByCodeIn(codes);
    }

    public Collection<ProductDto> findAll() {
        return catalogueProductRepository.findBy();
    }
}
