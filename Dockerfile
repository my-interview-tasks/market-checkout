FROM openjdk:8-jdk-alpine

EXPOSE 8083
RUN apk add --no-cache bash
ARG JAR_FILE=application/target/application-1.0-SNAPSHOT.jar

ADD ${JAR_FILE} app.jar

#  todo investigate wait-for-it.sh...
COPY wait-for-it.sh wait-for-it.sh
RUN chmod +x wait-for-it.sh


ENTRYPOINT ["./wait-for-it.sh", "postgresdb:5432", "--", "java", "-jar", "/app.jar"]
