package dlesiak.interview.test


import groovyx.net.http.RESTClient
import org.slf4j.LoggerFactory
import org.testcontainers.containers.DockerComposeContainer
import org.testcontainers.containers.output.Slf4jLogConsumer
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.spock.Testcontainers
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

@Testcontainers
@Stepwise
class CheckoutSpec extends Specification implements CheckoutApi {

    @Shared
    DockerComposeContainer checkoutApp = new DockerComposeContainer(new File("src/test/resources/docker-compose.yml"))
            .withLogConsumer("checkout", new Slf4jLogConsumer(LoggerFactory.getLogger(CheckoutSpec.class)))
            .waitingFor("checkout", Wait.forHttp("/products").forStatusCode(200))

    @Shared
    def client = new RESTClient(getURI())


    @Shared
    def firstCustomer = "customerFirst" //TODO do more tests multiconsumers

    def "list all products"() {
        Thread.sleep(10 * 1000)
        //TODO FIXME Without sleeping for some time, tests are executed before application is up. Setting wait policy does not help. Investigate.
        when:
        def response = client.get(path: '/products')

        then:
        response.status == 200

        and: "there are 4 products"
        response.data.size == 4
    }

    def "firstCustomer opens basket"() {
        when:
        openBasketFor(firstCustomer)

        then:
        basketIsOpened(firstCustomer)
    }

    def "firstCustomer adds products"() {
        when: "customer add product"
        def total = addProducts(firstCustomer, productCode, quantity)

        then:
        total == expectedTotal

        //TODO more readeable assertions
        where:
        productCode      | quantity | expectedTotal
        "product-tea"    | 1        | 40
        "product-tea"    | 2        | 80

        "product-coffee" | 2        | 80 + 15
        "product-coffee" | 3        | 80 + 15 + 10

        "product-tea"    | 3        | 70 + 25
        "product-tea"    | 5        | 70 + 2 * 40 + 25

        "product-vodka"  | 5        | 70 + 2 * 40 + 25 + 60 + 30
    }

    def "firstCustomer remove TEA"() {
        when: "customer remove product"
        def total = removeProduct(firstCustomer, "product-tea")

        then:
        total == 15 + 10 + 60 + 30
    }

    def "firstCustomer adds TEA again"() {
        when: "customer add product"
        def total = addProducts(firstCustomer, "product-tea", 5)

        then:
        total == 15 + 10 + 60 + 30 + (70 + 2 * 40)
    }

    def "customer checkout"() {
        when:
        def data = checkout(firstCustomer)

        then:
        //TODO more readeable assertions
        //TODO test returned receipt
        data.totalAfterDiscount == 15 + 10 + 60 + 30 + (70 + 2 * 40)
        data.total == 380
        noOpenBasket(firstCustomer)
        1 == closedBaskets(firstCustomer)

    }


    def getURI() {
        def host = checkoutApp.getServiceHost("checkout", 8083)
        return "http://${host}:8085"
    }
}

