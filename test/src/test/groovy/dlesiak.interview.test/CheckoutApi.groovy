package dlesiak.interview.test

import groovyx.net.http.HttpResponseException

import static groovyx.net.http.ContentType.JSON

trait CheckoutApi {

    //TODO
    def closedBaskets(String customerId) {
        def response = client.get(
                path: '/basket',
                query: [
                        customerId: customerId,
                        status    : "CLOSED"
                ]
        )
        return response.data.size
    }

    def noOpenBasket(String customerId) {
        try {
            def response = client.get(
                    path: '/basket/current',
                    query: [customerId: customerId]
            )
            return false
        } catch (HttpResponseException e) {
            if (e.statusCode == 404) {
                return true
            } else {
                throw e
            }
        }

    }

    def checkout(String customerId) {
        def response = client.post(
                path: '/basket/checkout',
                query: [customerId: customerId]
        )
        return response.data
    }

    def openBasketFor(String customerId) {
        def response = client.put(
                path: '/basket',
                query: [customerId: customerId]
        )

    }


    def basketIsOpened(String customerId) {
        def response = client.get(
                path: '/basket',
                query: [
                        customerId: customerId,
                        status    : "OPENED"
                ]
        )
        return response.status == 200
    }

    def addProducts(String customer, String product, Integer quantity) {
        def response = client.post(
                path: '/basket/item',
                body: [
                        productId : product,
                        quantity  : quantity,
                        customerId: customer
                ],
                requestContentType: JSON
        )
        return response.data.value
    }

    def removeProduct(String customer, String product) {
        def response = client.delete(
                path: '/basket/item',
                query: [
                        productCode: product,
                        customerId : customer
                ],
        )
        return response.data.value
    }

}
